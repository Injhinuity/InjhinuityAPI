﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;

namespace InjhinuityAPI.Extentions
{
    public static class Extentions
    {
        /// <summary>
        /// Acts as a mediator between the discovery DI and asp.net's already existing DI. Adds a custom 
        /// dictionary into a pre-defined collection to inject into the API.
        /// </summary>
        /// <param name="collection"></param>
        /// <param name="dict"></param>
        public static void AddMultipleFromDict(this IServiceCollection collection, ConcurrentDictionary<Type, object> dict)
        {
            foreach (KeyValuePair<Type, object> pair in dict)
                collection.Add(new ServiceDescriptor(pair.Key, pair.Value));
        }
    }
}
