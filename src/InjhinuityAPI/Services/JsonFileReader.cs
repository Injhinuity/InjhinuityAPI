using InjhinuityAPI.Services.Interfaces;
using System.IO;

namespace InjhinuityAPI.Services
{
    /// <summary>
    /// Simple file reader.
    /// </summary>
    public class JsonFileReader : IFileReader
    {
        public JsonFileReader() { }

        /// <summary>
        /// Returns the contents of a given file as a string.
        /// </summary>
        /// <param name="filePath">Path of file to read.</param>
        /// <returns>File contents as a string.</returns>
        public string ReadFile(string filePath)
        {
            using (StreamReader sr = new StreamReader(new FileStream(filePath, FileMode.OpenOrCreate)))
                return sr.ReadToEnd();
        }
    }
}
