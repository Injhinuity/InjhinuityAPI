﻿using InjhinuityAPI.Database.Config;
using InjhinuityAPI.Database.Context;
using InjhinuityAPI.Database.UnitOfWork;
using Microsoft.EntityFrameworkCore;

namespace InjhinuityAPI.Services
{
    /// <summary>
    /// Provides access to the UnitOfWork, which will execute all of the database related operations.
    /// Also initialises the database and ensures the latest migrations are applied.
    /// </summary>
    public class DbService : IDbService
    {
        private readonly DbContextOptions _options;

        /// <summary>
        /// Fetches the connection string and initialises a new DbContextOptions to later
        /// use once we create the UnitOfWork and execute requests to the database.
        /// </summary>
        /// <param name="config">Config instance containing the connection string.</param>
        public DbService(IAPIConfig config)
        {
            var optionsBuilder = new DbContextOptionsBuilder();
            optionsBuilder.UseSqlite(config.Db.ConnectionString);
            _options = optionsBuilder.Options;
        }

        public InjhinuityAPIContext GetDbContext()
        {
            var context = new InjhinuityAPIContext(_options);
            // Applies every migration to the current database. If changes are
            // missing from before, they will be applied here. 
            context.Database.Migrate();

            return context;
        }

        public IUnitOfWork UnitOfWork =>
            new UnitOfWork(GetDbContext());
    }
}