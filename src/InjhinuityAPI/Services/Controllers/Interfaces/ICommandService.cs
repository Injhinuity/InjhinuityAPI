﻿using InjhinuityAPI.Dto.Discord;
using InjhinuityAPI.Services.Injection;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace InjhinuityAPI.Services.Controllers.Interfaces
{
    public interface ICommandService : IService
    {
        Task<IEnumerable<CommandDto>> GetAllAsync(ulong guildID);
        Task<CommandDto> GetAsync(ulong guildID, string name);
        Task CreateAsync(CommandDto command);
        Task UpdateAsync(CommandDto command);
        Task DeleteAsync(CommandDto command);
    }
}
