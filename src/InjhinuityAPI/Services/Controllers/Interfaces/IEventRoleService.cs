﻿using InjhinuityAPI.Dto.Discord;
using InjhinuityAPI.Services.Injection;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace InjhinuityAPI.Services.Controllers.Interfaces
{
    public interface IEventRoleService : IService
    {
        Task<IEnumerable<EventRoleDto>> GetAllAsync();
        Task<EventRoleDto> GetAsync(ulong guildID);
        Task CreateAsync(EventRoleDto eventRole);
        Task UpdateAsync(EventRoleDto eventRole);
        Task DeleteAsync(EventRoleDto eventRole);
    }
}
