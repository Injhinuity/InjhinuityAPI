﻿using InjhinuityAPI.Dto.Discord;
using InjhinuityAPI.Services.Injection;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace InjhinuityAPI.Services.Controllers.Interfaces
{
    public interface IUserMessageService : IService
    {
        Task<IEnumerable<UserMessageDto>> GetAllAsync(ulong guildID);
        Task<UserMessageDto> GetAsync(ulong guildID, ulong messageID, ulong authorID);
        Task CreateAsync(UserMessageDto userMessage);
        Task UpdateAsync(UserMessageDto userMessage);
        Task DeleteAsync(UserMessageDto userMessage);
    }
}
