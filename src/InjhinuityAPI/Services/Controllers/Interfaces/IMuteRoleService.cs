﻿using InjhinuityAPI.Dto.Discord;
using InjhinuityAPI.Services.Injection;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace InjhinuityAPI.Services.Controllers.Interfaces
{
    public interface IMuteRoleService : IService
    {
        Task<IEnumerable<MuteRoleDto>> GetAllAsync();
        Task<MuteRoleDto> GetAsync(ulong guildID);
        Task CreateAsync(MuteRoleDto muteRole);
        Task UpdateAsync(MuteRoleDto muteRole);
        Task DeleteAsync(MuteRoleDto muteRole);
    }
}
