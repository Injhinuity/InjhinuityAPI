﻿using InjhinuityAPI.Dto.Discord;
using InjhinuityAPI.Services.Injection;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace InjhinuityAPI.Services.Controllers.Interfaces
{
    public interface IReactionService : IService
    {
        Task<IEnumerable<ReactionDto>> GetAllAsync(ulong guildID);
        Task<ReactionDto> GetAsync(ulong guildID, string name);
        Task CreateAsync(ReactionDto reaction);
        Task UpdateAsync(ReactionDto reaction);
        Task DeleteAsync(ReactionDto reaction);
    }
}
