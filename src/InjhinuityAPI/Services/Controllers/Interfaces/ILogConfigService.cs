﻿using InjhinuityAPI.Dto.Discord;
using InjhinuityAPI.Services.Injection;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace InjhinuityAPI.Services.Controllers.Interfaces
{
    public interface ILogConfigService : IService
    {
        Task<IEnumerable<LogConfigDto>> GetAllAsync();
        Task<LogConfigDto> GetAsync(ulong guildID);
        Task CreateAsync(LogConfigDto logConfig);
        Task UpdateAsync(LogConfigDto logConfig);
        Task DeleteAsync(LogConfigDto logConfig);
    }
}
