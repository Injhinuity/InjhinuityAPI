﻿using InjhinuityAPI.Dto.Discord;
using InjhinuityAPI.Services.Injection;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace InjhinuityAPI.Services.Controllers.Interfaces
{
    public interface IEventService : IService
    {
        Task<IEnumerable<EventDto>> GetAllAsync(ulong guildID);
        Task<EventDto> GetAsync(ulong guildID, string name);
        Task CreateAsync(EventDto @event);
        Task UpdateAsync(EventDto @event);
        Task DeleteAsync(EventDto @event);
    }
}
