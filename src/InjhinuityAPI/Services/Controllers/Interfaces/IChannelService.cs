﻿using InjhinuityAPI.Dto.Discord;
using InjhinuityAPI.Services.Injection;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace InjhinuityAPI.Services.Controllers.Interfaces
{
    public interface IChannelService : IService
    {
        Task<IEnumerable<ChannelDto>> GetAllAsync(ulong guildID);
        Task<ChannelDto> GetAsync(ulong guildID, string shortName);
        Task CreateAsync(ChannelDto command);
        Task UpdateAsync(ChannelDto command);
        Task DeleteAsync(ChannelDto command);
    }
}
