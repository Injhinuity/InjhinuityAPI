﻿using InjhinuityAPI.Dto.Discord;
using InjhinuityAPI.Services.Injection;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace InjhinuityAPI.Services.Controllers.Interfaces
{
    public interface IGuildService : IService
    {
        Task<IEnumerable<GuildDto>> GetAllAsync();
        Task<GuildDto> GetAsync(ulong guildID);
        Task CreateAsync(GuildDto guild);
        Task UpdateAsync(GuildDto guild);
        Task DeleteAsync(GuildDto guild);
    }
}
