﻿using InjhinuityAPI.Dto.Discord;
using InjhinuityAPI.Services.Injection;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace InjhinuityAPI.Services.Controllers.Interfaces
{
    public interface IParamService : IService
    {
        Task<IEnumerable<ParamDto>> GetAllAsync(ulong guildID);
        Task<ParamDto> GetAsync(ulong guildID, string shortName);
        Task CreateAsync(ParamDto param);
        Task UpdateAsync(ParamDto param);
        Task DeleteAsync(ParamDto param);
    }
}
