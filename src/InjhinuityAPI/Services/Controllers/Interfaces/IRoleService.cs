﻿using InjhinuityAPI.Dto.Discord;
using InjhinuityAPI.Services.Injection;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace InjhinuityAPI.Services.Controllers.Interfaces
{
    public interface IRoleService : IService
    {
        Task<IEnumerable<RoleDto>> GetAllAsync(ulong guildID);
        Task<RoleDto> GetAsync(ulong guildID, string name);
        Task CreateAsync(RoleDto role);
        Task UpdateAsync(RoleDto role);
        Task DeleteAsync(RoleDto role);
    }
}
