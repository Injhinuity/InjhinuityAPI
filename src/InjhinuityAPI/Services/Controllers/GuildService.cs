﻿using InjhinuityAPI.Dto.Discord;
using InjhinuityAPI.Services.Controllers.Interfaces;
using InjhinuityAPI.Services.Interfaces;
using InjhinuityAPI.Domain.Discord;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InjhinuityAPI.Services.Controllers
{
    public class GuildService : DiscordObjectService<Guild, GuildDto>, IGuildService
    {
        public GuildService(IDiscordMappingService mapper, IDbService dbService) : base(mapper, dbService)
        {
        }

        public async Task<IEnumerable<GuildDto>> GetAllAsync()
        {
            using (var uow = _dbService.UnitOfWork)
            {
                var guilds = await uow.GuildRepo.GetAllAsync();
                return guilds.Select(x => GetDto(x));
            }
        }

        public async Task<GuildDto> GetAsync(ulong guildID)
        {
            using (var uow = _dbService.UnitOfWork)
            {
                var domainObj = await uow.GuildRepo.ForAsync(guildID);
                return GetDto(domainObj);
            }
        }

        public async Task CreateAsync(GuildDto guild)
        {
            using (var uow = _dbService.UnitOfWork)
            {
                await uow.GuildRepo.ForAsync(guild.GuildID);
                await uow.CompleteAsync();
            }
        }

        public Task UpdateAsync(GuildDto guild) =>
            throw new NotImplementedException();

        public async Task DeleteAsync(GuildDto guild)
        {
            using (var uow = _dbService.UnitOfWork)
            {
                var domainObj = await uow.GuildRepo.GetAsync(guild.GuildID);
                uow.GuildRepo.Remove(domainObj);
                await uow.CompleteAsync();
            }
        }
    }
}
