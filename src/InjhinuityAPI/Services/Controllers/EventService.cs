﻿using InjhinuityAPI.Dto.Discord;
using InjhinuityAPI.Services.Controllers.Interfaces;
using InjhinuityAPI.Services.Interfaces;
using InjhinuityAPI.Domain.Discord;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InjhinuityAPI.Services.Controllers
{
    public class EventService : DiscordObjectService<Event, EventDto>, IEventService
    {
        public EventService(IDiscordMappingService mapper, IDbService dbService) : base(mapper, dbService)
        {
        }

        public async Task<IEnumerable<EventDto>> GetAllAsync(ulong guildID)
        {
            using (var uow = _dbService.UnitOfWork)
            {
                var events = await uow.EventRepo.GetAllAsync(guildID);
                return events.Select(x => GetDto(x));
            }
        }

        public async Task<EventDto> GetAsync(ulong guildID, string name)
        {
            using (var uow = _dbService.UnitOfWork)
            {
                var domainObj = await uow.EventRepo.GetByNameAsync(guildID, name);
                return GetDto(domainObj);
            }
        }

        public async Task CreateAsync(EventDto @event)
        {
            using (var uow = _dbService.UnitOfWork)
            {
                var domainObj = GetDomain(@event);
                await uow.EventRepo.AddAsync(domainObj);
                await uow.CompleteAsync();
            }
        }

        public async Task UpdateAsync(EventDto @event)
        {
            using (var uow = _dbService.UnitOfWork)
            {
                var domainObj = await uow.EventRepo.GetByNameAsync(@event.GuildID, @event.Name);

                domainObj.Description = @event.Description;
                domainObj.Date = @event.Date;
                domainObj.Duration = @event.Duration;
                domainObj.TimeZone = @event.TimeZone;
                domainObj.Started = @event.Started;

                uow.EventRepo.Update(domainObj);
                await uow.CompleteAsync();
            }
        }

        public async Task DeleteAsync(EventDto @event)
        {
            using (var uow = _dbService.UnitOfWork)
            {
                var domainObj = await uow.EventRepo.GetByNameAsync(@event.GuildID, @event.Name);
                uow.EventRepo.Remove(domainObj);
                await uow.CompleteAsync();
            }
        }
    }
}
