﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using InjhinuityAPI.Domain.Discord;
using InjhinuityAPI.Dto.Discord;
using InjhinuityAPI.Services.Controllers.Interfaces;
using InjhinuityAPI.Services.Interfaces;

namespace InjhinuityAPI.Services.Controllers
{
    public class MuteRoleService : DiscordObjectService<MuteRole, MuteRoleDto>, IMuteRoleService
    {
        public MuteRoleService(IDiscordMappingService mapper, IDbService dbService)
            : base(mapper, dbService)
        {
        }

        public async Task<IEnumerable<MuteRoleDto>> GetAllAsync()
        {
            using (var uow = _dbService.UnitOfWork)
            {
                var muteRoles = await uow.MuteRoleRepo.GetAllAsync();
                return muteRoles.Select(x => GetDto(x));
            }
        }

        public async Task<MuteRoleDto> GetAsync(ulong guildID)
        {
            using (var uow = _dbService.UnitOfWork)
            {
                var muteRole = await uow.MuteRoleRepo.GetAsync(guildID);
                return GetDto(muteRole);
            }
        }

        public Task CreateAsync(MuteRoleDto muteRole) =>
            throw new NotImplementedException();

        public async Task UpdateAsync(MuteRoleDto muteRole)
        {
            using (var uow = _dbService.UnitOfWork)
            {
                var domainObject = await uow.MuteRoleRepo.GetAsync(muteRole.GuildID);

                domainObject.RoleID = muteRole.RoleID;

                uow.MuteRoleRepo.Update(domainObject);
                await uow.CompleteAsync();
            }
        }

        public Task DeleteAsync(MuteRoleDto muteRole) =>
            throw new NotImplementedException();
    }
}
