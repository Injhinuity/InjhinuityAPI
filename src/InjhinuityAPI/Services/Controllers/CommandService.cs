﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using InjhinuityAPI.Dto.Discord;
using InjhinuityAPI.Services.Controllers.Interfaces;
using InjhinuityAPI.Services.Interfaces;
using InjhinuityAPI.Domain.Discord;

namespace InjhinuityAPI.Services.Controllers
{
    public class CommandService : DiscordObjectService<Command, CommandDto>, ICommandService
    {
        public CommandService(IDiscordMappingService mapper, IDbService dbService) : base(mapper, dbService)
        {
        }

        public async Task<IEnumerable<CommandDto>> GetAllAsync(ulong guildID)
        {
            using (var uow = _dbService.UnitOfWork)
            {
                var commands = await uow.CommandRepo.GetAllAsync(guildID);
                return commands.Select(x => GetDto(x));
            }
        }

        public async Task<CommandDto> GetAsync(ulong guildID, string name)
        {
            using (var uow = _dbService.UnitOfWork)
            {
                var command = await uow.CommandRepo.GetByNameAsync(guildID, name);
                return GetDto(command);
            }
        }

        public async Task CreateAsync(CommandDto command)
        {
            using (var uow = _dbService.UnitOfWork)
            {
                var domainObj = GetDomain(command);
                await uow.CommandRepo.AddAsync(domainObj);
                await uow.CompleteAsync();
            }
        }

        public async Task UpdateAsync(CommandDto command)
        {
            using (var uow = _dbService.UnitOfWork)
            {
                var domainObj = await uow.CommandRepo.GetByNameAsync(command.GuildID, command.Name);

                domainObj.Body = command.Body;

                uow.CommandRepo.Update(domainObj);
                await uow.CompleteAsync();
            }
        }

        public async Task DeleteAsync(CommandDto command)
        {
            using (var uow = _dbService.UnitOfWork)
            {
                var domainObj = await uow.CommandRepo.GetByNameAsync(command.GuildID, command.Name);

                uow.CommandRepo.Remove(domainObj);
                await uow.CompleteAsync();
            }
        }
    }
}
