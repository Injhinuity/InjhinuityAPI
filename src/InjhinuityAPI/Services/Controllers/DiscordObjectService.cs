﻿using InjhinuityAPI.Domain.DiscordObjects;
using InjhinuityAPI.Dto.Discord;
using InjhinuityAPI.Services.Interfaces;

namespace InjhinuityAPI.Services.Controllers
{
    public class DiscordObjectService<T, K>
        where T : IDiscordObject 
        where K : IDiscordObjectDto
    {
        private readonly IDiscordMappingService _mapper;
        protected readonly IDbService _dbService;

        public DiscordObjectService(IDiscordMappingService mapper, IDbService dbService)
        {
            _mapper = mapper;
            _dbService = dbService;
        }

        protected K GetDto(T obj) =>
            _mapper.ToDto<K>(obj);

        protected T GetDomain(K obj) =>
            _mapper.ToDomain<T>(obj);
    }
}
