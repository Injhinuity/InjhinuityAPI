﻿using InjhinuityAPI.Dto.Discord;
using InjhinuityAPI.Services.Controllers.Interfaces;
using InjhinuityAPI.Services.Interfaces;
using InjhinuityAPI.Domain.Discord;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InjhinuityAPI.Services.Controllers
{
    public class RoleService : DiscordObjectService<Role, RoleDto>, IRoleService
    {
        public RoleService(IDiscordMappingService mapper, IDbService dbService) : base(mapper, dbService)
        {
        }

        public async Task<IEnumerable<RoleDto>> GetAllAsync(ulong guildID)
        {
            using (var uow = _dbService.UnitOfWork)
            {
                var roles = await uow.RoleRepo.GetAllAsync(guildID);
                return roles.Select(x => GetDto(x));
            }
        }

        public async Task<RoleDto> GetAsync(ulong guildID, string name)
        {
            using (var uow = _dbService.UnitOfWork)
            {
                var domainObj = await uow.RoleRepo.GetByNameAsync(guildID, name);
                return GetDto(domainObj);
            }
        }

        public async Task CreateAsync(RoleDto role)
        {
            using (var uow = _dbService.UnitOfWork)
            {
                var domainObj = GetDomain(role);
                await uow.RoleRepo.AddAsync(domainObj);
                await uow.CompleteAsync();
            }
        }

        public async Task UpdateAsync(RoleDto role)
        {
            using (var uow = _dbService.UnitOfWork)
            {
                var domainObj = await uow.RoleRepo.GetByNameAsync(role.GuildID, role.Name);

                domainObj.RoleID = role.RoleID;

                uow.RoleRepo.Update(domainObj);
                await uow.CompleteAsync();
            }
        }

        public async Task DeleteAsync(RoleDto role)
        {
            using (var uow = _dbService.UnitOfWork)
            {
                var domainObj = await uow.RoleRepo.GetByNameAsync(role.GuildID, role.Name);
                uow.RoleRepo.Remove(domainObj);
                await uow.CompleteAsync();
            }
        }
    }
}
