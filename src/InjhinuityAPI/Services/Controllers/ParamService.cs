﻿using InjhinuityAPI.Dto.Discord;
using InjhinuityAPI.Services.Controllers.Interfaces;
using InjhinuityAPI.Services.Interfaces;
using InjhinuityAPI.Domain.Discord;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System;

namespace InjhinuityAPI.Services.Controllers
{
    public class ParamService : DiscordObjectService<Param, ParamDto>, IParamService
    {
        public ParamService(IDiscordMappingService mapper, IDbService dbService) : base(mapper, dbService)
        {
        }

        public async Task<IEnumerable<ParamDto>> GetAllAsync(ulong guildID)
        {
            using (var uow = _dbService.UnitOfWork)
            {
                var @params = await uow.ParamRepo.GetAllAsync(guildID);
                return @params.Select(x => GetDto(x));
            }
        }

        public async Task<ParamDto> GetAsync(ulong guildID, string shortName)
        {
            using (var uow = _dbService.UnitOfWork)
            {
                var domainObj = await uow.ParamRepo.GetByShortNameAsync(guildID, shortName);
                return GetDto(domainObj);
            }
        }

        public Task CreateAsync(ParamDto param) =>
            throw new NotImplementedException();

        public async Task UpdateAsync(ParamDto param)
        {
            using (var uow = _dbService.UnitOfWork)
            {
                var domainObj = await uow.ParamRepo.GetByShortNameAsync(param.GuildID, param.ShortName);

                if (domainObj.Value == "")
                    domainObj.Value = GetToggleValue(domainObj);
                else
                    domainObj.Value = param.Value;

                uow.ParamRepo.Update(domainObj);
                await uow.CompleteAsync();
            }
        }

        public Task DeleteAsync(ParamDto param) =>
            throw new NotImplementedException();

        private string GetToggleValue(Param domainObj) =>
            (domainObj.Value == "" || domainObj.Value == bool.FalseString).ToString();
    }
}
