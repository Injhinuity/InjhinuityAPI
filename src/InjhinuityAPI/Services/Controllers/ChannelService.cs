﻿using InjhinuityAPI.Dto.Discord;
using InjhinuityAPI.Services.Controllers.Interfaces;
using InjhinuityAPI.Services.Interfaces;
using InjhinuityAPI.Domain.Discord;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InjhinuityAPI.Services.Controllers
{
    public class ChannelService : DiscordObjectService<Channel, ChannelDto>, IChannelService
    {
        public ChannelService(IDiscordMappingService mapper, IDbService dbService) : base(mapper, dbService)
        {
        }

        public async Task<IEnumerable<ChannelDto>> GetAllAsync(ulong guildID)
        {
            using (var uow = _dbService.UnitOfWork)
            {
                var channels = await uow.ChannelRepo.GetAllAsync(guildID);
                return channels.Select(x => GetDto(x));
            }
        }

        public async Task<ChannelDto> GetAsync(ulong guildID, string shortName)
        {
            using (var uow = _dbService.UnitOfWork)
            {
                var channel = await uow.ChannelRepo.GetByShortNameAsync(guildID, shortName);
                return GetDto(channel);
            }
        }

        public async Task CreateAsync(ChannelDto channel)
        {
            using (var uow = _dbService.UnitOfWork)
            {
                var domainObj = GetDomain(channel);
                await uow.ChannelRepo.AddAsync(domainObj);
                await uow.CompleteAsync();
            }
        }

        public async Task UpdateAsync(ChannelDto channel)
        {
            using (var uow = _dbService.UnitOfWork)
            {
                var domainObj = await uow.ChannelRepo.GetByShortNameAsync(channel.GuildID, channel.ShortName);

                //TODO: Make service to "fill in" updateable fields
                domainObj.ChannelID = channel.ChannelID;

                uow.ChannelRepo.Update(domainObj);
                await uow.CompleteAsync();
            }
        }

        public async Task DeleteAsync(ChannelDto channel)
        {
            using (var uow = _dbService.UnitOfWork)
            {
                var domainObj = await uow.ChannelRepo.GetByShortNameAsync(channel.GuildID, channel.ShortName);
                uow.ChannelRepo.Remove(domainObj);
                await uow.CompleteAsync();
            }
        }
    }
}
