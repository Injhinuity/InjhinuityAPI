﻿using InjhinuityAPI.Domain.Discord;
using InjhinuityAPI.Dto.Discord;
using InjhinuityAPI.Services.Controllers.Interfaces;
using InjhinuityAPI.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InjhinuityAPI.Services.Controllers
{
    public class EventRoleService : DiscordObjectService<EventRole, EventRoleDto>, IEventRoleService
    {
        public EventRoleService(IDiscordMappingService mapper, IDbService dbService) : base(mapper, dbService)
        {
        }

        public async Task<IEnumerable<EventRoleDto>> GetAllAsync()
        {
            using (var uow = _dbService.UnitOfWork)
            {
                var eventRoles = await uow.EventRoleRepo.GetAllAsync();
                return eventRoles.Select(x => GetDto(x));
            }
        }

        public async Task<EventRoleDto> GetAsync(ulong guildID)
        {
            using (var uow = _dbService.UnitOfWork)
            {
                var eventRole = await uow.EventRoleRepo.GetAsync(guildID);
                return GetDto(eventRole);
            }
        }

        public Task CreateAsync(EventRoleDto eventRole) =>
            throw new NotImplementedException();

        public async Task UpdateAsync(EventRoleDto eventRole)
        {
            using (var uow = _dbService.UnitOfWork)
            {
                var domainObject = await uow.EventRoleRepo.GetAsync(eventRole.GuildID);

                domainObject.RoleID = eventRole.RoleID;

                uow.EventRoleRepo.Update(domainObject);
                await uow.CompleteAsync();
            }
        }

        public Task DeleteAsync(EventRoleDto eventRole) =>
            throw new NotImplementedException();
    }
}
