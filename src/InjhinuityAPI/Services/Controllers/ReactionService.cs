﻿using InjhinuityAPI.Dto.Discord;
using InjhinuityAPI.Services.Controllers.Interfaces;
using InjhinuityAPI.Services.Interfaces;
using InjhinuityAPI.Domain.Discord;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InjhinuityAPI.Services.Controllers
{
    public class ReactionService : DiscordObjectService<Reaction, ReactionDto>, IReactionService
    {
        public ReactionService(IDiscordMappingService mapper, IDbService dbService) : base(mapper, dbService)
        {
        }

        public async Task<IEnumerable<ReactionDto>> GetAllAsync(ulong guildID)
        {
            using (var uow = _dbService.UnitOfWork)
            {
                var reactions = await uow.ReactionRepo.GetAllAsync(guildID);
                return reactions.Select(x => GetDto(x));
            }
        }

        public async Task<ReactionDto> GetAsync(ulong guildID, string name)
        {
            using (var uow = _dbService.UnitOfWork)
            {
                var domainObj = await uow.ReactionRepo.GetByNameAsync(guildID, name);
                return GetDto(domainObj);
            }
        }

        public async Task CreateAsync(ReactionDto reaction)
        {
            using (var uow = _dbService.UnitOfWork)
            {
                var domainObj = GetDomain(reaction);
                await uow.ReactionRepo.AddAsync(domainObj);
                await uow.CompleteAsync();
            }
        }

        public async Task UpdateAsync(ReactionDto reaction)
        {
            using (var uow = _dbService.UnitOfWork)
            {
                var domainObj = await uow.ReactionRepo.GetByNameAsync(reaction.GuildID, reaction.Name);

                domainObj.Body = reaction.Body;

                uow.ReactionRepo.Update(domainObj);
                await uow.CompleteAsync();
            }
        }

        public async Task DeleteAsync(ReactionDto reaction)
        {
            using (var uow = _dbService.UnitOfWork)
            {
                var domainObj = await uow.ReactionRepo.GetByNameAsync(reaction.GuildID, reaction.Name);
                uow.ReactionRepo.Remove(domainObj);
                await uow.CompleteAsync();
            }
        }
    }
}
