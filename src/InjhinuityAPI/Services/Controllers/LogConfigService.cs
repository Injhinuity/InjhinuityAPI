﻿using InjhinuityAPI.Dto.Discord;
using InjhinuityAPI.Services.Controllers.Interfaces;
using InjhinuityAPI.Services.Interfaces;
using InjhinuityAPI.Domain.Discord;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InjhinuityAPI.Services.Controllers
{
    public class LogConfigService : DiscordObjectService<LogConfig, LogConfigDto>, ILogConfigService
    {
        public LogConfigService(IDiscordMappingService mapper, IDbService dbService) : base(mapper, dbService)
        {
        }

        public async Task<IEnumerable<LogConfigDto>> GetAllAsync()
        {
            using (var uow = _dbService.UnitOfWork)
            {
                var configs = await uow.LogConfigRepo.GetAllAsync();
                return configs.Select(x => GetDto(x));
            }
        }

        public async Task<LogConfigDto> GetAsync(ulong guildID)
        {
            using (var uow = _dbService.UnitOfWork)
            {
                var domainObj = await uow.LogConfigRepo.GetAsync(guildID);
                return GetDto(domainObj);
            }
        }

        public Task CreateAsync(LogConfigDto logConfig) =>
            throw new NotImplementedException();

        public async Task UpdateAsync(LogConfigDto logConfig)
        {
            using (var uow = _dbService.UnitOfWork)
            {
                var domainObj = await uow.LogConfigRepo.GetAsync(logConfig.GuildID);

                domainObj.LogFlagValue = logConfig.LogFlagValue;

                uow.LogConfigRepo.Update(domainObj);
                await uow.CompleteAsync();
            }
        }

        public Task DeleteAsync(LogConfigDto logConfig) =>
            throw new NotImplementedException();
    }
}
