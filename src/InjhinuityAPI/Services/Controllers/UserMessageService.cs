﻿using InjhinuityAPI.Dto.Discord;
using InjhinuityAPI.Services.Controllers.Interfaces;
using InjhinuityAPI.Services.Interfaces;
using InjhinuityAPI.Domain.Discord;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InjhinuityAPI.Services.Controllers
{
    public class UserMessageService : DiscordObjectService<UserMessage, UserMessageDto>, IUserMessageService
    {
        public UserMessageService(IDiscordMappingService mapper, IDbService dbService) : base(mapper, dbService)
        {
        }

        public async Task<IEnumerable<UserMessageDto>> GetAllAsync(ulong guildID)
        {
            using (var uow = _dbService.UnitOfWork)
            {
                var userMessages = await uow.UserMessageRepo.GetAllAsync(guildID);
                return userMessages.Select(x => GetDto(x));
            }
        }

        public async Task<UserMessageDto> GetAsync(ulong guildID, ulong messageID, ulong authorID)
        {
            using (var uow = _dbService.UnitOfWork)
            {
                var domainObj = await uow.UserMessageRepo.GetByMessageAndAuthorIDAsync(guildID, messageID, authorID);
                return GetDto(domainObj);
            }
        }

        public async Task CreateAsync(UserMessageDto userMessage)
        {
            using (var uow = _dbService.UnitOfWork)
            {
                var domainObj = GetDomain(userMessage);
                await uow.UserMessageRepo.AddAsync(domainObj);
                await uow.CompleteAsync();
            }
        }

        public async Task UpdateAsync(UserMessageDto userMessage)
        {
            using (var uow = _dbService.UnitOfWork)
            {
                var domainObj = await uow.UserMessageRepo.GetByMessageAndAuthorIDAsync(userMessage.GuildID, 
                                                                                        userMessage.MessageID, 
                                                                                        userMessage.AuthorID);

                domainObj.Content = userMessage.Content;

                uow.UserMessageRepo.Update(domainObj);
                await uow.CompleteAsync();
            }
        }

        public async Task DeleteAsync(UserMessageDto userMessage)
        {
            using (var uow = _dbService.UnitOfWork)
            {
                var domainObj = await uow.UserMessageRepo.GetByMessageAndAuthorIDAsync(userMessage.GuildID, 
                                                                                        userMessage.MessageID, 
                                                                                        userMessage.AuthorID);
                uow.UserMessageRepo.Remove(domainObj);
                await uow.CompleteAsync();
            }
        }
    }
}
