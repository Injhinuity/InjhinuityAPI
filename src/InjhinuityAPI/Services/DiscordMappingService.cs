﻿using AutoMapper;
using InjhinuityAPI.Domain.DiscordObjects;
using InjhinuityAPI.Dto.Discord;
using InjhinuityAPI.Services.Interfaces;
using InjhinuityAPI.Domain.Discord;

namespace InjhinuityAPI.Services
{
    public class DiscordMappingService : IDiscordMappingService
    {
        private IMapper _mapper;

        public DiscordMappingService()
        {
            _mapper = CreateMapper();
        }

        public T ToDomain<T>(IDiscordObjectDto obj) where T : IDiscordObject =>
            _mapper.Map<T>(obj);

        public T ToDto<T>(IDiscordObject obj) where T : IDiscordObjectDto =>
            _mapper.Map<T>(obj);

        private IMapper CreateMapper()
        {
            return new MapperConfiguration(x =>
            {
                #region DiscordObjects
                x.CreateMap<IDiscordObject, IDiscordObjectDto>()
                    .ReverseMap();

                x.CreateMap<DiscordObject, DiscordObjectDto>()
                    .ReverseMap();

                x.CreateMap<Channel, ChannelDto>()
                    .ReverseMap();

                x.CreateMap<Command, CommandDto>()
                    .ReverseMap();

                x.CreateMap<Event, EventDto>()
                    .ReverseMap();

                x.CreateMap<Guild, GuildDto>()
                    .ReverseMap();

                x.CreateMap<Param, ParamDto>()
                    .ReverseMap();

                x.CreateMap<LogConfig, LogConfigDto>()
                    .ReverseMap();

                x.CreateMap<Reaction, ReactionDto>()
                    .ReverseMap();

                x.CreateMap<Role, RoleDto>()
                    .ReverseMap();

                x.CreateMap<UserMessage, UserMessageDto>()
                    .ReverseMap();

                x.CreateMap<MuteRole, MuteRoleDto>()
                    .ReverseMap();

                x.CreateMap<EventRole, EventRoleDto>()
                    .ReverseMap();
                #endregion
            })
            .CreateMapper();
        }
    }
}
