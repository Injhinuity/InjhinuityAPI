﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Reflection;
using System.Diagnostics;
using System.Linq;
using System.Collections.Concurrent;
using InjhinuityAPI.Services.Injection;
using NLog;

namespace InjhinuityAPI.Services.Provider
{
    /// <summary>
    /// Builds the service collection using discovery of the <see cref="IService"/> interface. Can then be built
    /// and added to the API via dependency injection.
    /// </summary>
    public class BotServiceCollectionBuilder
    {
        private ConcurrentDictionary<Type, object> _dict = new ConcurrentDictionary<Type, object>();
        private readonly Logger _log;

        public BotServiceCollectionBuilder()
        {
            _log = LogManager.GetCurrentClassLogger();
        }

        public BotServiceCollectionBuilder AddManual<T>(T obj)
        {
            _dict.TryAdd(typeof(T), obj);
            return this;
        }

        /// <summary>
        /// Automatically discovers every class which implements a specific Interface (IService) and
        /// will inject it within the dependency container. It also resolves all cross-service 
        /// dependencies and takes care of initialising everything.
        /// </summary>
        /// <param name="assembly">Current assembly which we will discover services from.</param>
        /// <returns>Returns the builder instance.</returns>
        public ConcurrentDictionary<Type, object> LoadFrom(Assembly assembly)
        {
            /// Fetches all types and maps the ones which implement <see cref="IService"/>
            var allTypes = assembly.GetTypes();
            var services = new Queue<Type>(allTypes
                    .Where(x => x.GetInterfaces().Contains(typeof(IService))
                        && !x.GetTypeInfo().IsInterface && !x.GetTypeInfo().IsAbstract)
                    .ToArray());

            var interfaces = new HashSet<Type>(allTypes
                    .Where(x => x.GetInterfaces().Contains(typeof(IService))
                        && x.GetTypeInfo().IsInterface));

            var alreadyFailed = new Dictionary<Type, int>();

            var sw = Stopwatch.StartNew();
            var swInstance = new Stopwatch();
            while (services.Count > 0)
            {
                // Get a type I need to make an instance of
                var type = services.Dequeue();

                // If that type is already instantiated, skip
                if (_dict.TryGetValue(type, out _)) 
                    continue;

                // Get constructor argument types I need to pass in
                var ctor = type.GetConstructors()[0];
                var argTypes = ctor
                    .GetParameters()
                    .Select(x => x.ParameterType)
                    .ToArray();

                // Get constructor arguments from the dictionary of already instantiated types
                var args = new List<object>(argTypes.Length);
                foreach (var arg in argTypes) 
                {
                    // If I got current one, add it to the list of instances and move on
                    if (_dict.TryGetValue(arg, out var argObj)) 
                        args.Add(argObj);
                    // If I failed getting it, add it to the end, and break
                    else
                    {
                        services.Enqueue(type);
                        if (alreadyFailed.ContainsKey(type))
                        {
                            alreadyFailed[type]++;
                            // If instanciating takes too many attemps, log the issue and continue
                            if (alreadyFailed[type] > 3)
                                _log.Warn(type.Name + " wasn't instantiated in the first 3 attempts. Missing " + arg.Name + " type");
                        }
                        else
                            alreadyFailed.Add(type, 1);
                        break;
                    }
                }
                if (args.Count != argTypes.Length)
                    continue;
                _log.Info("Loading " + type.Name);
                swInstance.Restart();
                var instance = ctor.Invoke(args.ToArray());
                swInstance.Stop();
                if (swInstance.Elapsed.TotalSeconds > 5)
                    _log.Info($"{type.Name} took {swInstance.Elapsed.TotalSeconds:F2}s to load.");
                // If the type is an interface, add it as an interface with it's type
                var interfaceType = interfaces.FirstOrDefault(x => instance.GetType().GetInterfaces().Contains(x));
                if (interfaceType != null)
                    _dict.TryAdd(interfaceType, instance);

                // Otherwise, just add it with it's type
                _dict.TryAdd(type, instance);
            }
            sw.Stop();
            _log.Info($"All services loaded in {sw.Elapsed.TotalSeconds:F2}s");

            return _dict;
        }
    }
}
