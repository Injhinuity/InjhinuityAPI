﻿namespace InjhinuityAPI.Services.Injection
{
    /// <summary>
    /// Interface used for automatic service discovery by reflection. The ServiceProvider
    /// will find every class implementing this and will add it to the service container.
    /// </summary>
    public interface IService
    {
    }
}
