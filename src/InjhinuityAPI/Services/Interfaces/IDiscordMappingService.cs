﻿using InjhinuityAPI.Domain.DiscordObjects;
using InjhinuityAPI.Dto.Discord;
using InjhinuityAPI.Services.Injection;

namespace InjhinuityAPI.Services.Interfaces
{
    public interface IDiscordMappingService : IService
    {
        T ToDto<T>(IDiscordObject obj) where T : IDiscordObjectDto;
        T ToDomain<T>(IDiscordObjectDto obj) where T : IDiscordObject;
    }
}
