﻿using InjhinuityAPI.Services.Injection;

namespace InjhinuityAPI.Services.Interfaces
{
    public interface IFileReader : IService
    {
        string ReadFile(string path);
    }
}
