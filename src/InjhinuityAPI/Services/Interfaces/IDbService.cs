﻿using InjhinuityAPI.Database.Context;
using InjhinuityAPI.Database.UnitOfWork;
using InjhinuityAPI.Services.Injection;

namespace InjhinuityAPI.Services
{
    public interface IDbService : IService
    {
        InjhinuityAPIContext GetDbContext();
        IUnitOfWork UnitOfWork { get; }
    }
}