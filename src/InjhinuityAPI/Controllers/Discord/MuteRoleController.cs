﻿using InjhinuityAPI.Dto.Discord;
using InjhinuityAPI.Services.Controllers.Interfaces;
using InjhinuityAPI.Database.Config;
using InjhinuityAPI.Filters;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace InjhinuityAPI.Controllers.Discord
{
    /// <summary>
    /// API Controller that handles every api call for the <see cref="MuteRoleDto"/> discord object. Only GET/PUT
    /// is implemented.
    /// API route is api/discord/muterole/...
    /// </summary>
    [Authorize]
    [ValidateModel]
    [Produces("application/json")]
    [Route(APIConfig.BASE_API_ROUTE + APIConfig.DISCORD_API_ROUTE + "/[controller]")]
    public class MuteRoleController : DiscordController
    {
        private readonly IMuteRoleService _muteRoleService;

        public MuteRoleController(IMuteRoleService muteRoleService)
        {
            _muteRoleService = muteRoleService;
        }

        /// <summary>
        /// GET ALL .../discord/muterole/all
        /// </summary>
        [Route("all")]
        [HttpGet]
        public async Task<IActionResult> GetAll() =>
            Ok(await _muteRoleService.GetAllAsync());

        /// <summary>
        /// GET .../discord/muterole?{guildID}
        /// </summary>
        [HttpGet]
        [ValidateMuteRoleExists]
        public async Task<IActionResult> Get(ulong guildID) =>
            Ok(await _muteRoleService.GetAsync(guildID));

        /// <summary>
        /// PUT ...discord/muterole
        /// </summary>
        [HttpPut]
        [ValidateMuteRoleExists]
        public async Task<IActionResult> Put([FromBody] MuteRoleDto muteRoledto)
        {
            await _muteRoleService.UpdateAsync(muteRoledto);
            return Ok();
        }
    }
}
