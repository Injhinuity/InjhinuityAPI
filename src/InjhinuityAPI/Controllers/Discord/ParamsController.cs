﻿using InjhinuityAPI.Dto.Discord;
using InjhinuityAPI.Services.Controllers.Interfaces;
using InjhinuityAPI.Database.Config;
using InjhinuityAPI.Filters;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace InjhinuityAPI.Controllers.Discord
{
    /// <summary>
    /// API Controller that handles every api call for the <see cref="Param"/> discord object. Only GET/PUT 
    /// is implemented.
    /// API route is api/discord/params/...
    /// </summary>
    [Authorize]
    [ValidateModel]
    [Produces("application/json")]
    [Route(APIConfig.BASE_API_ROUTE + APIConfig.DISCORD_API_ROUTE + "/[controller]")]
    public class ParamsController : DiscordController
    {
        private readonly IParamService _paramService;

        public ParamsController(IParamService paramService)
        {
            _paramService = paramService;
        }

        /// <summary>
        /// GET .../discord/params/all?{guildID}
        /// </summary>
        [Route("all")]
        [HttpGet]
        [ValidateParamExists]
        public async Task<IActionResult> GetAll(ulong guildID) =>
            Ok(await _paramService.GetAllAsync(guildID));

        /// <summary>
        /// GET .../discord/params?{guildID}&{shortName}
        /// </summary>
        [HttpGet]
        [ValidateParamExists]
        public async Task<IActionResult> Get(ulong guildID, string shortName) =>
            Ok(await _paramService.GetAsync(guildID, shortName));

        /// <summary>
        /// PUT .../discord/params
        /// </summary>
        [HttpPut]
        [ValidateParamExists]
        public async Task<IActionResult> Put([FromBody] ParamDto paramdto)
        {
            await _paramService.UpdateAsync(paramdto);
            return Ok();
        }
    }
}
