﻿using InjhinuityAPI.Dto.Discord;
using InjhinuityAPI.Services.Controllers.Interfaces;
using InjhinuityAPI.Database.Config;
using InjhinuityAPI.Filters;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace InjhinuityAPI.Controllers.Discord
{
    /// <summary>
    /// API Controller that handles every api call for the <see cref="RoleDto"/> discsord object. GET/POST/PUT/DELETE
    /// is implemented.
    /// API route is api/discord/reactions/...
    /// </summary>
    [ValidateModel]
    [Produces("application/json")]
    [Route(APIConfig.BASE_API_ROUTE + APIConfig.DISCORD_API_ROUTE + "/[controller]")]
    public class RolesController : DiscordController
    {
        private readonly IRoleService _roleService;

        public RolesController(IRoleService roleService)
        {
            _roleService = roleService;
        }

        /// <summary>
        /// GET ...discord/roles/all?{guildID}
        /// </summary>
        [Route("all")]
        [HttpGet]
        public async Task<IActionResult> GetAll(ulong guildID) =>
            Ok(await _roleService.GetAllAsync(guildID));

        /// <summary>
        /// GET ...discord/roles?{guildID}&{name}
        /// </summary>
        [HttpGet]
        [ValidateRoleExists]
        public async Task<IActionResult> Get(ulong guildID, string name) =>
            Ok(await _roleService.GetAsync(guildID, name));

        /// <summary>
        /// POST ...discord/roles
        /// </summary>
        [HttpPost]
        [ValidateRoleDoesntExist]
        public async Task<IActionResult> Post([FromBody] RoleDto roledto)
        {
            await _roleService.CreateAsync(roledto);
            return Ok();
        }

        /// <summary>
        /// PUT ...discord/roles
        /// </summary>
        [HttpPut]
        [ValidateRoleExists]
        public async Task<IActionResult> Put([FromBody] RoleDto roledto)
        {
            await _roleService.UpdateAsync(roledto);
            return Ok();
        }

        /// <summary>
        /// DELETE ...discord/roles
        /// </summary>
        [HttpDelete]
        [ValidateRoleExists]
        public async Task<IActionResult> Delete([FromBody] RoleDto roledto)
        {
            await _roleService.DeleteAsync(roledto);
            return Ok();
        }
    }
}
