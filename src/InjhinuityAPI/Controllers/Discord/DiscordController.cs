﻿using Microsoft.AspNetCore.Mvc;

namespace InjhinuityAPI.Controllers.Discord
{
    /// <summary>
    /// API Base Controller for every Discord Controller. Provides a base "/discord" path.
    /// </summary>
    public abstract class DiscordController : Controller
    {
        public DiscordController()
        {
        }
    }
}
