﻿using InjhinuityAPI.Database.Config;
using InjhinuityAPI.Filters;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using InjhinuityAPI.Services.Controllers.Interfaces;
using InjhinuityAPI.Dto.Discord;

namespace InjhinuityAPI.Controllers.Discord
{
    /// <summary>
    /// API Controller that handles every api call for the <see cref="Reaction"/> discord object. GET/POST/PUT/DELETE
    /// is implemented.
    /// API route is api/discord/reactions/...
    /// </summary>
    [Authorize]
    [ValidateModel]
    [Produces("application/json")]
    [Route(APIConfig.BASE_API_ROUTE + APIConfig.DISCORD_API_ROUTE + "/[controller]")]
    public class ReactionsController : DiscordController
    {
        private readonly IReactionService _reactionService;

        public ReactionsController(IReactionService reactionService)
        {
            _reactionService = reactionService;
        }

        /// <summary>
        /// GET ...discord/reactions/all?{guildID}
        /// </summary>
        [Route("all")]
        [HttpGet]
        public async Task<IActionResult> GetAll(ulong guildID) =>
            Ok(await _reactionService.GetAllAsync(guildID));

        /// <summary>
        /// GET ...discord/reactions?{guildID}&{name}
        /// </summary>
        [HttpGet]
        [ValidateReactionExists]
        public async Task<IActionResult> Get(ulong guildID, string name) =>
            Ok(await _reactionService.GetAsync(guildID, name));

        /// <summary>
        /// POST ...discord/reactions
        /// </summary>
        [HttpPost]
        [ValidateReactionDoesntExist]
        public async Task<IActionResult> Post([FromBody] ReactionDto reactiondto)
        {
            await _reactionService.CreateAsync(reactiondto);
            return Ok();
        }

        /// <summary>
        /// PUT ...discord/reactions
        /// </summary>
        [HttpPut]
        [ValidateReactionExists]
        public async Task<IActionResult> Put([FromBody] ReactionDto reactiondto)
        {
            await _reactionService.UpdateAsync(reactiondto);
            return Ok();
        }

        /// <summary>
        /// DELETE ...discord/reactions
        /// </summary>
        [HttpDelete]
        [ValidateReactionExists]
        public async Task<IActionResult> Delete([FromBody] ReactionDto reactiondto)
        {
            await _reactionService.DeleteAsync(reactiondto);
            return Ok();
        }
    }
}
