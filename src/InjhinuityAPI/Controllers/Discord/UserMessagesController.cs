﻿using System.Threading.Tasks;
using InjhinuityAPI.Database.Config;
using Microsoft.AspNetCore.Mvc;
using InjhinuityAPI.Filters;
using Microsoft.AspNetCore.Authorization;
using InjhinuityAPI.Services.Controllers.Interfaces;
using InjhinuityAPI.Dto.Discord;

namespace InjhinuityAPI.Controllers.Discord
{
    /// <summary>
    /// API Controller that handles every api call for the <see cref="UserMessage"/> discord object. Only GET/POST
    /// is implemented.
    /// API route is api/discord/usermessages/...
    /// </summary>
    [Authorize]
    [ValidateModel]
    [Produces("application/json")]
    [Route(APIConfig.BASE_API_ROUTE + APIConfig.DISCORD_API_ROUTE + "/[controller]")]
    public class UserMessagesController : DiscordController
    {
        private readonly IUserMessageService _userMessageService;

        public UserMessagesController(IUserMessageService userMessageService)
        {
            _userMessageService = userMessageService;
        }

        /// <summary>
        /// GET .../discord/usermessages?{guildID}&{messageID}&{authorID}
        /// </summary>
        [HttpGet]
        [ValidateUserMessageExists]
        public async Task<IActionResult> Get(ulong guildID, ulong messageID, ulong authorID) =>
            Ok(await _userMessageService.GetAsync(guildID, messageID, authorID));

        /// <summary>
        /// POST .../discord/usermessages
        /// </summary>
        [HttpPost]
        [ValidateUserMessageDoesntExist]
        public async Task<IActionResult> Post([FromBody] UserMessageDto userMessagedto)
        {
            await _userMessageService.CreateAsync(userMessagedto);
            return Ok();
        }
    }
}
