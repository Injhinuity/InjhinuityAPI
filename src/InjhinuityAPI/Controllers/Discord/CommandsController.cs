﻿using Microsoft.AspNetCore.Mvc;
using InjhinuityAPI.Database.Config;
using InjhinuityAPI.Filters;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using InjhinuityAPI.Services.Controllers.Interfaces;
using InjhinuityAPI.Dto.Discord;

namespace InjhinuityAPI.Controllers.Discord
{
    /// <summary>
    /// API Controller that handles every api call for the <see cref="Command"/> discord object. GET/POST/PUT/DELETE
    /// is implemented.
    /// API route is api/discord/commands/...
    /// </summary>
    [Authorize]
    [ValidateModel]
    [Produces("application/json")]
    [Route(APIConfig.BASE_API_ROUTE + APIConfig.DISCORD_API_ROUTE + "/[controller]")]
    public class CommandsController : DiscordController
    {
        private readonly ICommandService _commandService;

        public CommandsController(ICommandService commandService)
        {
            _commandService = commandService;
        }

        /// <summary>
        /// GET ...discord/commands/all?{guildID}
        /// </summary>
        [Route("all")]
        [HttpGet]
        public async Task<IActionResult> GetAll(ulong guildID) =>
            Ok(await _commandService.GetAllAsync(guildID));

        /// <summary>
        /// GET ...discord/commands?{guildID}&{name}
        /// </summary>
        [HttpGet]
        [ValidateCommandExists]
        public async Task<IActionResult> Get(ulong guildID, string name) =>
            Ok(await _commandService.GetAsync(guildID, name));

        /// <summary>
        /// POST ...discord/commands
        /// </summary>
        [HttpPost]
        [ValidateCommandDoesntExist]
        public async Task<IActionResult> Post([FromBody] CommandDto commanddto)
        {
            await _commandService.CreateAsync(commanddto);
            return Ok();
        }
            

        /// <summary>
        /// PUT ...discord/commands
        /// </summary>
        [HttpPut]
        [ValidateCommandExists]
        public async Task<IActionResult> Put([FromBody] CommandDto commanddto)
        {
            await _commandService.UpdateAsync(commanddto);
            return Ok();
        }

        /// <summary>
        /// DELETE ...discord/commands
        /// </summary>
        [HttpDelete]
        [ValidateCommandExists]
        public async Task<IActionResult> Delete([FromBody] CommandDto commanddto)
        {
            await _commandService.DeleteAsync(commanddto);
            return Ok();
        }
    }
}