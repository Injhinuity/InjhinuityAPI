﻿using InjhinuityAPI.Database.Config;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using InjhinuityAPI.Filters;
using InjhinuityAPI.Services.Controllers.Interfaces;
using InjhinuityAPI.Dto.Discord;

namespace InjhinuityAPI.Controllers.Discord
{
    /// <summary>
    /// API Controller that handles every api call for the <see cref="Guild"/> discord object. Only GET/POST/DELETE
    /// is implemented.
    /// API route is api/discord/guilds/...
    /// </summary>
    [Authorize]
    [ValidateModel]
    [Produces("application/json")]
    [Route(APIConfig.BASE_API_ROUTE + APIConfig.DISCORD_API_ROUTE + "/[controller]")]
    public class GuildsController : DiscordController
    {
        private readonly IGuildService _guildService;

        public GuildsController(IGuildService guildService)
        {
            _guildService = guildService;
        }

        /// <summary>
        /// GET .../discord/guildconfigs/all
        /// </summary>
        [Route("all")]
        [HttpGet]
        public async Task<IActionResult> GetAll() =>
            Ok(await _guildService.GetAllAsync());

        /// <summary>
        /// GET .../discord/guilds?{guildID}
        /// </summary>
        [HttpGet]
        public async Task<IActionResult> Get(ulong guildID) =>
            Ok(await _guildService.GetAsync(guildID));

        /// <summary>
        /// POST .../discord/guilds?{guildID}
        /// </summary>
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] GuildDto guilddto)
        {
            await _guildService.CreateAsync(guilddto);
            return Ok();
        }

        /// <summary>
        /// DELETE ...discord/guilds
        /// </summary>
        [HttpDelete]
        public async Task<IActionResult> Delete([FromBody] GuildDto guilddto)
        {
            await _guildService.DeleteAsync(guilddto);
            return Ok();
        }
    }
}
