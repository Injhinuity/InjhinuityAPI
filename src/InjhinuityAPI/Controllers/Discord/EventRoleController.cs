﻿using InjhinuityAPI.Database.Config;
using InjhinuityAPI.Dto.Discord;
using InjhinuityAPI.Filters;
using InjhinuityAPI.Services.Controllers.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace InjhinuityAPI.Controllers.Discord
{
    /// <summary>
    /// API Controller that handles every api call for the <see cref="EventRole"/> discord object. Only GET/PUT
    /// is implemented.
    /// API route is api/discord/eventrole/...
    /// </summary>
    [Authorize]
    [ValidateModel]
    [Produces("application/json")]
    [Route(APIConfig.BASE_API_ROUTE + APIConfig.DISCORD_API_ROUTE + "/[controller]")]
    public class EventRoleController : DiscordController
    {
        private readonly IEventRoleService _eventRoleService;

        public EventRoleController(IEventRoleService eventRoleService)
        {
            _eventRoleService = eventRoleService;
        }

        /// <summary>
        /// GET ALL .../discord/eventrole/all
        /// </summary>
        [Route("all")]
        [HttpGet]
        public async Task<IActionResult> GetAll() =>
            Ok(await _eventRoleService.GetAllAsync());

        /// <summary>
        /// GET .../discord/eventrole?{guildID}
        /// </summary>
        [HttpGet]
        [ValidateEventRoleExists]
        public async Task<IActionResult> Get(ulong guildID) =>
            Ok(await _eventRoleService.GetAsync(guildID));

        /// <summary>
        /// PUT ...discord/eventrole
        /// </summary>
        [HttpPut]
        [ValidateEventRoleExists]
        public async Task<IActionResult> Put([FromBody] EventRoleDto eventRoledto)
        {
            await _eventRoleService.UpdateAsync(eventRoledto);
            return Ok();
        }
    }
}
