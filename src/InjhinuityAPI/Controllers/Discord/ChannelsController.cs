﻿using InjhinuityAPI.Database.Config;
using InjhinuityAPI.Filters;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using InjhinuityAPI.Services.Controllers.Interfaces;
using InjhinuityAPI.Dto.Discord;

namespace InjhinuityAPI.Controllers.Discord
{
    /// <summary>
    /// API Controller that handles every api call for the <see cref="Channel"/> discord object. Only GET/PUT
    /// is implemented.
    /// API route is api/discord/channels/...
    /// </summary>
    [Authorize]
    [ValidateModel]
    [Produces("application/json")]
    [Route(APIConfig.BASE_API_ROUTE + APIConfig.DISCORD_API_ROUTE + "/[controller]")]
    public class ChannelsController : DiscordController
    {
        private readonly IChannelService _channelService;

        public ChannelsController(IChannelService channelService)
        {
            _channelService = channelService;
        }

        /// <summary>
        /// GET ...discord/channels/all?{guildID}
        /// </summary>
        [Route("all")]
        [HttpGet]
        public async Task<IActionResult> GetAll(ulong guildID) =>
            Ok(await _channelService.GetAllAsync(guildID));

        /// <summary>
        /// GET .../discord/channels?{guildID}&{shortName}
        /// </summary>
        [HttpGet]
        [ValidateChannelExists]
        public async Task<IActionResult> Get(ulong guildID, string shortName) =>
            Ok(await _channelService.GetAsync(guildID, shortName));

        /// <summary>
        /// PUT .../discord/channels
        /// </summary>
        [HttpPut]
        [ValidateChannelExists]
        public async Task<IActionResult> Put([FromBody] ChannelDto channeldto)
        {
            await _channelService.UpdateAsync(channeldto);
            return Ok();
        }
    }
}
