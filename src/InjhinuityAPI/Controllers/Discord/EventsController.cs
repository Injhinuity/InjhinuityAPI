﻿using InjhinuityAPI.Dto.Discord;
using InjhinuityAPI.Services.Controllers.Interfaces;
using InjhinuityAPI.Database.Config;
using InjhinuityAPI.Filters;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace InjhinuityAPI.Controllers.Discord
{
    /// <summary>
    /// API Controller that handles every api call for the <see cref="Event"/> discord object. GET/POST/PUT/DELETE
    /// is implemented.
    /// API route is api/discord/events/...
    /// </summary>
    [Authorize]
    [ValidateModel]
    [Produces("application/json")]
    [Route(APIConfig.BASE_API_ROUTE + APIConfig.DISCORD_API_ROUTE + "/[controller]")]
    public class EventsController : DiscordController
    {
        private readonly IEventService _eventService;

        public EventsController(IEventService eventService)
        {
            _eventService = eventService;
        }

        /// <summary>
        /// GET ...discord/events/all?{guildID}
        /// </summary>
        [Route("all")]
        [HttpGet]
        public async Task<IActionResult> GetAll(ulong guildID) =>
            Ok(await _eventService.GetAllAsync(guildID));

        /// <summary>
        /// GET ...discord/events?{guildID}&{name}&{date}
        /// </summary>
        [HttpGet]
        [ValidateEventExists]
        public async Task<IActionResult> Get(ulong guildID, string name) =>
            Ok(await _eventService.GetAsync(guildID, name));


        /// <summary>
        /// POST ...discord/events
        /// </summary>
        [HttpPost]
        [ValidateEventDoesntExist]
        public async Task<IActionResult> Post([FromBody] EventDto eventdto)
        {
            await _eventService.CreateAsync(eventdto);
            return Ok();
        }

        /// <summary>
        /// PUT ...discord/events
        /// </summary>
        [HttpPut]
        [ValidateEventExists]
        public async Task<IActionResult> Put([FromBody] EventDto eventdto)
        {
            await _eventService.UpdateAsync(eventdto);
            return Ok();
        }

        /// <summary>
        /// DELETE ...discord/events?{guildID}&{name}
        /// </summary>
        [HttpDelete]
        [ValidateEventExists]
        public async Task<IActionResult> Delete([FromBody] EventDto eventdto)
        {
            await _eventService.DeleteAsync(eventdto);
            return Ok();
        }
    }
}
