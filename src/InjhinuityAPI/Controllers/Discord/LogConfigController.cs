﻿using InjhinuityAPI.Dto.Discord;
using InjhinuityAPI.Services.Controllers.Interfaces;
using InjhinuityAPI.Database.Config;
using InjhinuityAPI.Filters;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace InjhinuityAPI.Controllers.Discord
{
    /// <summary>
    /// API Controller that handles every api call for the <see cref="LogConfig"/> discord object. Only GET/PUT
    /// is implemented.
    /// API route is api/discord/logconfig/...
    /// </summary>
    [Authorize]
    [ValidateModel]
    [Produces("application/json")]
    [Route(APIConfig.BASE_API_ROUTE + APIConfig.DISCORD_API_ROUTE + "/[controller]")]
    public class LogConfigController : DiscordController
    {
        private readonly ILogConfigService _logConfigService;

        public LogConfigController(ILogConfigService logConfigService)
        {
            _logConfigService = logConfigService;
        }

        /// <summary>
        /// GET ALL .../discord/logconfig/all
        /// </summary>
        [Route("all")]
        [HttpGet]
        public async Task<IActionResult> GetAll() =>
            Ok(await _logConfigService.GetAllAsync());

        /// <summary>
        /// GET .../discord/logconfig?{guildID}
        /// </summary>
        [HttpGet]
        [ValidateLogConfigExists]
        public async Task<IActionResult> Get(ulong guildID) =>
            Ok(await _logConfigService.GetAsync(guildID));

        /// <summary>
        /// PUT ...discord/logconfig
        /// </summary>
        [HttpPut]
        [ValidateLogConfigExists]
        public async Task<IActionResult> Put([FromBody] LogConfigDto logConfigdto)
        {
            await _logConfigService.UpdateAsync(logConfigdto);
            return Ok();
        }
    }
}
