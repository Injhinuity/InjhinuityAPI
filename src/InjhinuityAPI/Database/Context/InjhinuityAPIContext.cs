﻿using InjhinuityAPI.Database.Config;
using InjhinuityAPI.Domain.Discord;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace InjhinuityAPI.Database.Context
{
    /// <summary>
    /// Factory class of the API Context class. Will take care of building a
    /// new context on launch.
    /// </summary>
    public class InjhinuityContextFactory : IDesignTimeDbContextFactory<InjhinuityAPIContext>
    {
        public InjhinuityAPIContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder()
                .UseSqlite(APIConfig.DbFileName);
            return new InjhinuityAPIContext(builder.Options);
        }
    }

    /// <summary>
    /// Context class of the API. Will define every set of object that will be stored
    /// and used by client applications.
    /// </summary>
    public class InjhinuityAPIContext : DbContext
    {
        public DbSet<Guild> Guilds { get; set; }
        public DbSet<Command> GuildCommands { get; set; }
        public DbSet<Event> GuildEvents { get; set; }
        public DbSet<Param> GuildParams { get; set; }
        public DbSet<Channel> GuildChannels { get; set; }
        public DbSet<Reaction> GuildReactions { get; set; }
        public DbSet<UserMessage> UserMessages { get; set; }
        public DbSet<LogConfig> LogConfigs { get; set; }
        public DbSet<Role> GuildRoles { get; set; }
        public DbSet<MuteRole> MuteRoles { get; set; }
        public DbSet<EventRole> EventRoles { get; set; }

        public InjhinuityAPIContext(DbContextOptions options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            #region ModelBuilder
            modelBuilder.Entity<Guild>()
                .HasKey(x => x.GuildID);

            modelBuilder.Entity<Command>()
                .HasKey(x => new { x.GuildID, x.Name });

            modelBuilder.Entity<Event>()
                .HasKey(x => new { x.GuildID, x.Name });

            modelBuilder.Entity<Param>()
                .HasKey(x => new { x.GuildID, x.ShortName });

            modelBuilder.Entity<Channel>()
                .HasKey(x => new { x.GuildID, x.ShortName });

            modelBuilder.Entity<Reaction>()
                .HasKey(x => new { x.GuildID, x.Name });

            modelBuilder.Entity<UserMessage>()
                .HasKey(x => new { x.GuildID, x.MessageID });

            modelBuilder.Entity<LogConfig>()
                .HasKey(x => new { x.GuildID });

            modelBuilder.Entity<Role>()
                .HasKey(x => new { x.GuildID, x.Name });

            modelBuilder.Entity<MuteRole>()
                .HasKey(x => new { x.GuildID });

            modelBuilder.Entity<EventRole>()
                .HasKey(x => new { x.GuildID });
            #endregion
        }
    }
}
