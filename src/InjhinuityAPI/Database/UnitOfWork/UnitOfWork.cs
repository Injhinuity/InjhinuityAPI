﻿using InjhinuityAPI.Database.Context;
using InjhinuityAPI.Database.Repositories.Interfaces;
using InjhinuityAPI.Database.Repositories.SQLite;
using System;
using System.Threading.Tasks;

namespace InjhinuityAPI.Database.UnitOfWork
{
    /// <summary>
    /// Class that contains the current context and repositories. Acts as a data
    /// access layer for the backend.
    /// </summary>
    public class UnitOfWork : IUnitOfWork
    {
        public InjhinuityAPIContext _context { get; }

        private IGuildRepository _guildConfigs;
        public IGuildRepository GuildRepo => _guildConfigs ?? (_guildConfigs = new GuildRepository(_context));

        private ICommandRepository _commands;
        public ICommandRepository CommandRepo => _commands ?? (_commands = new CommandRepository(_context));

        private IEventRepository _events;
        public IEventRepository EventRepo => _events ?? (_events = new EventRepository(_context));

        private IChannelRepository _channels;
        public IChannelRepository ChannelRepo => _channels ?? (_channels = new ChannelRepository(_context));

        private IParamRepository _params;
        public IParamRepository ParamRepo => _params ?? (_params = new ParamRepository(_context));

        private IUserMessageRepository _messages;
        public IUserMessageRepository UserMessageRepo => _messages ?? (_messages = new UserMessageRepository(_context));

        private IReactionRepository _reactions;
        public IReactionRepository ReactionRepo => _reactions ?? (_reactions = new ReactionRepository(_context));

        private ILogConfigRepository _logConfigs;
        public ILogConfigRepository LogConfigRepo => _logConfigs ?? (_logConfigs = new LogConfigRepository(_context));

        private IRoleRepository _roles;
        public IRoleRepository RoleRepo => _roles ?? (_roles = new RoleRepository(_context));

        private IMuteRoleRepository _muteRoles;
        public IMuteRoleRepository MuteRoleRepo => _muteRoles ?? (_muteRoles = new MuteRoleRepository(_context));

        private IEventRoleRepository _eventRoles;
        public IEventRoleRepository EventRoleRepo => _eventRoles ?? (_eventRoles = new EventRoleRepository(_context));

        private bool disposed = false;

        public UnitOfWork(InjhinuityAPIContext context) => 
            _context = context;

        public int Complete() =>
            _context.SaveChanges();

        public Task<int> CompleteAsync() =>
            _context.SaveChangesAsync();

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected void Dispose(bool disposing)
        {
            if (!disposed)
                if (disposing)
                    _context.Dispose();

            disposed = true;
        }
    }
}
