﻿using InjhinuityAPI.Database.Context;
using InjhinuityAPI.Database.Repositories.Interfaces;
using System;
using System.Threading.Tasks;

namespace InjhinuityAPI.Database.UnitOfWork
{
    public interface IUnitOfWork : IDisposable
    {
        InjhinuityAPIContext _context { get; }

        IGuildRepository GuildRepo { get; }
        ICommandRepository CommandRepo { get; }
        IEventRepository EventRepo { get; }
        IChannelRepository ChannelRepo { get; }
        IParamRepository ParamRepo { get; }
        IReactionRepository ReactionRepo { get; }
        IUserMessageRepository UserMessageRepo { get; }
        ILogConfigRepository LogConfigRepo { get; }
        IRoleRepository RoleRepo { get; }
        IMuteRoleRepository MuteRoleRepo { get; }
        IEventRoleRepository EventRoleRepo { get; }

        int Complete();
        Task<int> CompleteAsync();
    }
}