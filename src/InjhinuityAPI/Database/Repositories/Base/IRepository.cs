﻿using InjhinuityAPI.Domain.Discord;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace InjhinuityAPI.Database.Repositories.Base
{
    /// <summary>
    /// Base repository with basic Add, Remove and Update functions.
    /// </summary>
    /// <typeparam name="T">Type of object to handle.</typeparam>
    public interface IRepository<T> where T : DiscordObject
    {
        T Get(ulong id);
        Task<T> GetAsync(ulong guildID);
        IEnumerable<T> GetAll(ulong guildID);
        Task<IEnumerable<T>> GetAllAsync(ulong guildID);

        void Add(T obj);
        void AddRange(params T[] objs);
        Task AddAsync(T obj);

        void Remove(ulong guildID);
        void Remove(T obj);
        void RemoveRange(params T[] objs);

        void Update(T obj);
        void UpdateRange(params T[] objs);
    }
}
