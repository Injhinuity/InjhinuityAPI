﻿using InjhinuityAPI.Domain.Discord;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InjhinuityAPI.Database.Repositories.Base
{
    /// <summary>
    /// Simple implementation of a repository patern. Uses CRUD operations to modify
    /// every kind of DBSet available. Can also act on lists/collections.
    /// </summary>
    /// <typeparam name="T">Type of object to be handled by the repository.</typeparam>
    public class Repository<T> : IRepository<T> where T : DiscordObject
    {
        protected DbContext _context;
        protected DbSet<T> _set;

        public Repository(DbContext context)
        {
            _context = context;
            _set = context.Set<T>();
        }

        public T Get(ulong guildID) =>
            _set.FirstOrDefault(e => e.GuildID == guildID);

        public async Task<T> GetAsync(ulong guildID) =>
            await _set.FirstOrDefaultAsync(e => e.GuildID == guildID);

        public IEnumerable<T> GetAll(ulong guildID) =>
            _set.Where(e => e.GuildID == guildID).ToList();

        public async Task<IEnumerable<T>> GetAllAsync(ulong guildID) =>
            await _set.Where(e => e.GuildID == guildID).ToListAsync();

        public void Add(T obj) =>
           _set.Add(obj);

        public Task AddAsync(T obj) =>
            _set.AddAsync(obj);

        public void AddRange(params T[] objs) =>
            _set.AddRange(objs);

        public void Remove(ulong id) =>
            _set.Remove(Get(id));

        public void Remove(T obj) =>
            _set.Remove(obj);

        public void RemoveRange(params T[] objs) =>
            _set.RemoveRange(objs);

        public void Update(T obj) =>
            _set.Update(obj);

        public void UpdateRange(params T[] objs) =>
            _set.UpdateRange(objs);
    }
}
