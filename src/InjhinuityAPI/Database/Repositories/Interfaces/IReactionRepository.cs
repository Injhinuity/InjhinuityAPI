﻿using InjhinuityAPI.Database.Repositories.Base;
using InjhinuityAPI.Domain.Discord;
using System.Threading.Tasks;

namespace InjhinuityAPI.Database.Repositories.Interfaces
{
    public interface IReactionRepository : IRepository<Reaction>
    {
        Task<Reaction> GetByNameAsync(ulong guildID, string name);
    }
}
