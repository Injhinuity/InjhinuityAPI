﻿using InjhinuityAPI.Database.Repositories.Base;
using InjhinuityAPI.Domain.Discord;
using System.Threading.Tasks;

namespace InjhinuityAPI.Database.Repositories.Interfaces
{
    public interface IUserMessageRepository : IRepository<UserMessage>
    {
        Task<UserMessage> GetByMessageAndAuthorIDAsync(ulong guildID, ulong messageID, ulong authorID);
    }
}
