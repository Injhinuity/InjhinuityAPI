﻿using InjhinuityAPI.Database.Repositories.Base;
using InjhinuityAPI.Domain.Discord;
using System.Threading.Tasks;

namespace InjhinuityAPI.Database.Repositories.Interfaces
{
    public interface IParamRepository : IRepository<Param>
    {
        Task<Param> GetByShortNameAsync(ulong guildID, string name);
    }
}
