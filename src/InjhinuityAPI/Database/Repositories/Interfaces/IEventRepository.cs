﻿using InjhinuityAPI.Database.Repositories.Base;
using InjhinuityAPI.Domain.Discord;
using System.Threading.Tasks;

namespace InjhinuityAPI.Database.Repositories.Interfaces
{
    public interface IEventRepository : IRepository<Event>
    {
        Task<Event> GetByNameAsync(ulong guildID, string name);
    }
}
