﻿using InjhinuityAPI.Database.Repositories.Base;
using InjhinuityAPI.Domain.Discord;
using System.Threading.Tasks;

namespace InjhinuityAPI.Database.Repositories.Interfaces
{
    public interface ICommandRepository : IRepository<Command>
    {
        Task<Command> GetByNameAsync(ulong guildID, string name);
    }
}
