﻿using InjhinuityAPI.Database.Repositories.Base;
using InjhinuityAPI.Domain.Discord;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace InjhinuityAPI.Database.Repositories.Interfaces
{
    public interface IMuteRoleRepository : IRepository<MuteRole>
    {
        Task<IEnumerable<MuteRole>> GetAllAsync();
    }
}
