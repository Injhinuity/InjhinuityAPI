﻿using InjhinuityAPI.Database.Repositories.Base;
using InjhinuityAPI.Domain.Discord;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace InjhinuityAPI.Database.Repositories.Interfaces
{
    public interface IEventRoleRepository : IRepository<EventRole>
    {
        Task<IEnumerable<EventRole>> GetAllAsync();
    }
}
