﻿using InjhinuityAPI.Database.Repositories.Base;
using InjhinuityAPI.Domain.Discord;
using System.Threading.Tasks;

namespace InjhinuityAPI.Database.Repositories.Interfaces
{
    public interface IChannelRepository : IRepository<Channel>
    {
        Task<Channel> GetByShortNameAsync(ulong guildID, string shortName);
    }
}
