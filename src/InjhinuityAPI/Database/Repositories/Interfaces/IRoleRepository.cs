﻿using InjhinuityAPI.Database.Repositories.Base;
using InjhinuityAPI.Domain.Discord;
using System.Threading.Tasks;

namespace InjhinuityAPI.Database.Repositories.Interfaces
{
    public interface IRoleRepository : IRepository<Role>
    {
        Task<Role> GetByNameAsync(ulong guildID, string name);
    }
}
