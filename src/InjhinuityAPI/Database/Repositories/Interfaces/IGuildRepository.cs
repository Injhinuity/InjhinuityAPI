﻿using InjhinuityAPI.Database.Repositories.Base;
using InjhinuityAPI.Domain.Discord;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InjhinuityAPI.Database.Repositories.Interfaces
{
    public interface IGuildRepository : IRepository<Guild>
    {
        Task<Guild> ForAsync(ulong guildID, Func<DbSet<Guild>, IQueryable<Guild>> includes = null);
        Task<IEnumerable<Guild>> GetAllAsync();
    }
}
