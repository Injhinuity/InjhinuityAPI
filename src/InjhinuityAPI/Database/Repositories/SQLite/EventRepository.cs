﻿using InjhinuityAPI.Database.Repositories.Base;
using InjhinuityAPI.Database.Repositories.Interfaces;
using InjhinuityAPI.Domain.Discord;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace InjhinuityAPI.Database.Repositories.SQLite
{
    /// <summary>
    /// SQLite implementation of the <see cref="Event"/> repository.
    /// </summary>
    public class EventRepository : Repository<Event>, IEventRepository
    {
        public EventRepository(DbContext context) : base(context)
        {
        }

        public async Task<Event> GetByNameAsync(ulong guildID, string name) =>
            await _set.FirstOrDefaultAsync(x => x.GuildID == guildID && 
                                            x.Name == name);
    }
}
