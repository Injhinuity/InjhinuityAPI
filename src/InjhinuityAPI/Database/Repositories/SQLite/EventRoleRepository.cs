﻿using InjhinuityAPI.Database.Repositories.Base;
using InjhinuityAPI.Database.Repositories.Interfaces;
using InjhinuityAPI.Domain.Discord;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace InjhinuityAPI.Database.Repositories.SQLite
{
    public class EventRoleRepository : Repository<EventRole>, IEventRoleRepository
    {
        public EventRoleRepository(DbContext context) : base(context)
        {
        }

        public async Task<IEnumerable<EventRole>> GetAllAsync() =>
            await _set.ToListAsync();
    }
}
