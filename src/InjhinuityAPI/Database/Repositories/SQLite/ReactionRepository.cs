﻿using System.Threading.Tasks;
using InjhinuityAPI.Database.Repositories.Base;
using InjhinuityAPI.Database.Repositories.Interfaces;
using InjhinuityAPI.Domain.Discord;
using Microsoft.EntityFrameworkCore;

namespace InjhinuityAPI.Database.Repositories.SQLite
{
    /// <summary>
    /// SQLite implementation of the <see cref="Reaction"/> repository.
    /// </summary>
    public class ReactionRepository : Repository<Reaction>, IReactionRepository
    {
        public ReactionRepository(DbContext context) : base(context)
        {
        }

        public async Task<Reaction> GetByNameAsync(ulong guildID, string name) =>
            await _set.FirstOrDefaultAsync(x => x.GuildID == guildID && x.Name == name);
    }
}
