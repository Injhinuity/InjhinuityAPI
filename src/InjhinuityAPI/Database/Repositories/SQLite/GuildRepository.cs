﻿using InjhinuityAPI.Database.Repositories.Base;
using InjhinuityAPI.Database.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using InjhinuityAPI.Domain.Discord;

namespace InjhinuityAPI.Database.Repositories.SQLite
{
    /// <summary>
    /// SQLite implementation of the <see cref="Guild"/> repository.
    /// </summary>
    public class GuildRepository : Repository<Guild>, IGuildRepository
    {
        //TODO: Reinclude command cooldowns
        public GuildRepository(DbContext context) : base(context)
        {
        }

        public async Task<Guild> ForAsync(ulong guildID, Func<DbSet<Guild>, IQueryable<Guild>> includes = null)
        {
            Guild config;

            if (includes == null)
            {
                config = _set
                //.Include(gc => gc.CommandCooldowns)
                .Include(gc => gc.GuildCommands)
                .Include(gc => gc.GuildEvents)
                .Include(gc => gc.GuildReactions)
                .Include(gc => gc.GuildParams)
                .Include(gc => gc.GuildChannels)
                .Include(gc => gc.UserMessages)
                .Include(gc => gc.LogConfig)
                .Include(gc => gc.GuildRoles)
                .FirstOrDefault(c => c.GuildID == guildID);
            }
            else
            {
                var set = includes(_set);
                config = set
                //.Include(gc => gc.CommandCooldowns)
                .Include(gc => gc.GuildCommands)
                .Include(gc => gc.GuildEvents)
                .Include(gc => gc.GuildReactions)
                .Include(gc => gc.GuildParams)
                .Include(gc => gc.GuildChannels)
                .Include(gc => gc.UserMessages)
                .Include(gc => gc.LogConfig)
                .Include(gc => gc.GuildRoles)
                .FirstOrDefault(c => c.GuildID == guildID);
            }

            if (config == null)
            {
                config = new Guild { GuildID = guildID };
                _set.Add(config);
                config.InitGuildConfig();
                await _context.SaveChangesAsync();
            }

            return config;
        }

        public async Task<IEnumerable<Guild>> GetAllAsync() =>
            await _set
                //.Include(gc => gc.CommandCooldowns)
                .Include(gc => gc.GuildCommands)
                .Include(gc => gc.GuildEvents)
                .Include(gc => gc.GuildReactions)
                .Include(gc => gc.GuildParams)
                .Include(gc => gc.GuildChannels)
                .Include(gc => gc.UserMessages)
                .Include(gc => gc.LogConfig)
                .Include(gc => gc.GuildRoles)
                .ToListAsync();
    }
}
