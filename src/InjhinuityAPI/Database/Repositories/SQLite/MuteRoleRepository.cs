﻿using System.Collections.Generic;
using System.Threading.Tasks;
using InjhinuityAPI.Database.Repositories.Base;
using InjhinuityAPI.Database.Repositories.Interfaces;
using InjhinuityAPI.Domain.Discord;
using Microsoft.EntityFrameworkCore;

namespace InjhinuityAPI.Database.Repositories.SQLite
{
    public class MuteRoleRepository : Repository<MuteRole>, IMuteRoleRepository
    {
        public MuteRoleRepository(DbContext context) : base(context)
        {
        }

        public async Task<IEnumerable<MuteRole>> GetAllAsync() =>
            await _set.ToListAsync();
    }
}
