﻿using System.Collections.Generic;
using System.Threading.Tasks;
using InjhinuityAPI.Database.Repositories.Base;
using InjhinuityAPI.Database.Repositories.Interfaces;
using InjhinuityAPI.Domain.Discord;
using Microsoft.EntityFrameworkCore;

namespace InjhinuityAPI.Database.Repositories.SQLite
{
    /// <summary>
    /// SQLite implementation of the <see cref="LogConfig"/> repository.
    /// </summary>
    public class LogConfigRepository : Repository<LogConfig>, ILogConfigRepository
    {
        public LogConfigRepository(DbContext context) : base(context)
        {
        }

        public async Task<IEnumerable<LogConfig>> GetAllAsync() =>
            await _set.ToListAsync();
    }
}
