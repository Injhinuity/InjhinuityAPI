﻿using InjhinuityAPI.Database.Repositories.Base;
using InjhinuityAPI.Database.Repositories.Interfaces;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using InjhinuityAPI.Domain.Discord;

namespace InjhinuityAPI.Database.Repositories.SQLite
{
    /// <summary>
    /// SQLite implementation of the <see cref="Channel"/> repository.
    /// </summary>
    public class ChannelRepository : Repository<Channel>, IChannelRepository
    {
        public ChannelRepository(DbContext context) : base(context)
        {
        }

        public async Task<Channel> GetByShortNameAsync(ulong guildID, string shortName) =>
            await _set.FirstOrDefaultAsync(x => x.GuildID == guildID && x.ShortName == shortName);
    }
}
