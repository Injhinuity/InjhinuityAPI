﻿using InjhinuityAPI.Database.Repositories.Base;
using InjhinuityAPI.Database.Repositories.Interfaces;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using InjhinuityAPI.Domain.Discord;

namespace InjhinuityAPI.Database.Repositories.SQLite
{
    /// <summary>
    /// SQLite implementation of the <see cref="Param"/> repository.
    /// </summary>
    public class ParamRepository : Repository<Param>, IParamRepository
    {
        public ParamRepository(DbContext context) : base(context)
        {
        }

        public async Task<Param> GetByShortNameAsync(ulong guildID, string shortName) =>
            await _set.FirstOrDefaultAsync(x => x.GuildID == guildID && x.ShortName == shortName);
    }
}
