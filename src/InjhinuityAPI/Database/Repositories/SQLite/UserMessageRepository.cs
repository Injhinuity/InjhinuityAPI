﻿using InjhinuityAPI.Database.Repositories.Base;
using InjhinuityAPI.Database.Repositories.Interfaces;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using InjhinuityAPI.Domain.Discord;

namespace InjhinuityAPI.Database.Repositories.SQLite
{
    /// <summary>
    /// SQLite implementation of the <see cref="UserMessage"/> repository.
    /// </summary>
    public class UserMessageRepository : Repository<UserMessage>, IUserMessageRepository
    {
        public UserMessageRepository(DbContext context) : base(context)
        {
        }

        public async Task<UserMessage> GetByMessageAndAuthorIDAsync(ulong guildID, ulong messageID, ulong authorID) =>
            await _set.FirstOrDefaultAsync(x => x.GuildID == guildID && 
                                            x.MessageID == messageID && 
                                            x.AuthorID == authorID);
    }
}
