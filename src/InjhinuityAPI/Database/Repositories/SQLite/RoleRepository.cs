﻿using System.Threading.Tasks;
using InjhinuityAPI.Database.Repositories.Base;
using InjhinuityAPI.Database.Repositories.Interfaces;
using InjhinuityAPI.Domain.Discord;
using Microsoft.EntityFrameworkCore;

namespace InjhinuityAPI.Database.Repositories.SQLite
{
    /// <summary>
    /// SQLite implementation of the <see cref="Role"/> repository.
    /// </summary>
    public class RoleRepository : Repository<Role>, IRoleRepository
    {
        public RoleRepository(DbContext context) : base(context)
        {
        }

        public async Task<Role> GetByNameAsync(ulong guildID, string name) =>
            await _set.FirstOrDefaultAsync(x => x.GuildID == guildID && x.Name == name);
    }
}
