﻿using InjhinuityAPI.Database.Repositories.Base;
using Microsoft.EntityFrameworkCore;
using InjhinuityAPI.Database.Repositories.Interfaces;
using System.Threading.Tasks;
using InjhinuityAPI.Domain.Discord;

namespace InjhinuityAPI.Database.Repositories.SQLite
{
    /// <summary>
    /// SQLite implementation of the <see cref="Command"/> repository.
    /// </summary>
    public class CommandRepository : Repository<Command>, ICommandRepository
    {
        public CommandRepository(DbContext context) : base(context)
        {
        }

        public async Task<Command> GetByNameAsync(ulong guildID, string name) => 
            await _set.FirstOrDefaultAsync(x => x.GuildID == guildID && x.Name == name);
    }
}
