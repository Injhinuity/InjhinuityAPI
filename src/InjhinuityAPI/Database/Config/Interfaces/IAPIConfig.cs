﻿using InjhinuityAPI.Services.Injection;

namespace InjhinuityAPI.Database.Config
{
    public interface IAPIConfig : IService
    {
        DbConfig Db { get; }

        string ParamsPath { get; }
        string ChannelNamePath { get; }
    }
}
