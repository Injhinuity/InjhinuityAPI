﻿namespace InjhinuityAPI.Database.Config
{
    // dotnet ef migrations add (name)
    // dotnet ef database update

    /// <summary>
    /// Class that contains every configuration to start the API. Includes notably 
    /// the connection string and important file paths.
    /// </summary>
    public class APIConfig : IAPIConfig
    {
        public DbConfig Db { get; }
        public static string DbFileName => "Filename=InjhinuityAPI.db";
        public string ParamsPath => "/Json/guildParams.json";
        public string ChannelNamePath => "/Json/channelIDs.json";

        public const string BASE_API_ROUTE = "api";
        public const string DISCORD_API_ROUTE = "/discord";
        public const string CHANNEL_API_ROUTE = "channels";
        public const string COMMAND_API_ROUTE = "commands";
        public const string EVENT_API_ROUTE = "events";
        public const string GUILDCONFIG_API_ROUTE = "guildconfigs";
        public const string PARAM_API_ROUTE = "params";

        public APIConfig()
        {
            Db = new DbConfig("sqlite", DbFileName);
        }
    }
}
