﻿namespace InjhinuityAPI.Database.Config
{
    /// <summary>
    /// Simple datastore class for the database config. Contains the database type
    /// and the connection string for said database.
    /// </summary>
    public class DbConfig
    {
        public string Type { get; }
        public string ConnectionString { get; }

        public DbConfig(string type, string connString)
        {
            Type = type;
            ConnectionString = connString;
        }
    }
}
