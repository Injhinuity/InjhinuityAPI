﻿namespace InjhinuityAPI.Dto.Discord
{
    public class ParamDto : DiscordObjectDto
    {
        public string ShortName { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
    }
}
