﻿namespace InjhinuityAPI.Dto.Discord
{
    public abstract class DiscordObjectDto : IDiscordObjectDto
    {
        public ulong GuildID { get; set; }
    }
}
