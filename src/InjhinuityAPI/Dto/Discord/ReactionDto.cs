﻿namespace InjhinuityAPI.Dto.Discord
{
    public class ReactionDto : DiscordObjectDto
    {
        public string Name { get; set; }
        public string Body { get; set; }
    }
}
