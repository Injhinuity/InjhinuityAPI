﻿namespace InjhinuityAPI.Dto.Discord
{
    public class EventRoleDto : DiscordObjectDto
    {
        public string Name { get; set; }
        public ulong RoleID { get; set; }
    }
}
