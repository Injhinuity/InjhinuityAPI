﻿namespace InjhinuityAPI.Dto.Discord
{
    public class EventDto : DiscordObjectDto
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public long Date { get; set; }
        public string TimeZone { get; set; }
        public long Duration { get; set; }
        public bool Started { get; set; }
    }
}
