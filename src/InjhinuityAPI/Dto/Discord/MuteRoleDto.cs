﻿namespace InjhinuityAPI.Dto.Discord
{
    public class MuteRoleDto : DiscordObjectDto
    {
        public string Name { get; set; }
        public ulong RoleID { get; set; }
    }
}
