﻿namespace InjhinuityAPI.Dto.Discord
{
    public class ChannelDto : DiscordObjectDto
    {
        public string ShortName { get; set; }
        public string Name { get; set; }
        public ulong ChannelID { get; set; }
    }
}
