﻿namespace InjhinuityAPI.Dto.Discord
{
    public class CommandDto : DiscordObjectDto
    {
        public string Name { get; set; }
        public string Body { get; set; }
    }
}
