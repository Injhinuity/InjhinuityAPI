﻿namespace InjhinuityAPI.Dto.Discord
{
    public class LogConfigDto : DiscordObjectDto
    {
        public int LogFlagValue { get; set; }
    }
}
