﻿namespace InjhinuityAPI.Dto.Discord
{
    public class RoleDto : DiscordObjectDto
    {
        public string Name { get; set; }
        public ulong RoleID { get; set; }
    }
}
