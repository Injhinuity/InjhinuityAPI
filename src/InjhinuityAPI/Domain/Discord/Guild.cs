﻿using InjhinuityAPI.Services;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.IO;

namespace InjhinuityAPI.Domain.Discord
{
    /// <summary>
    /// Contains everything related to a particular guild, notably commands, events and parameters.
    /// This object is what distinguishes a guild from another within the bot.
    /// </summary>
    public class Guild : DiscordObject
    {
        public ICollection<Command> GuildCommands { get; set; } = new HashSet<Command>();
        public ICollection<Event> GuildEvents { get; set; } = new HashSet<Event>();
        public ICollection<Param> GuildParams { get; set; } = new HashSet<Param>();
        public ICollection<Channel> GuildChannels { get; set; } = new HashSet<Channel>();
        public ICollection<UserMessage> UserMessages { get; set; } = new HashSet<UserMessage>();
        public ICollection<Reaction> GuildReactions { get; set; } = new HashSet<Reaction>();
        public ICollection<Role> GuildRoles { get; set; } = new HashSet<Role>();

        //public ICollection<CommandCooldown> CommandCooldowns { get; set; } = new HashSet<CommandCooldown>();

        public LogConfig LogConfig { get; set; } = new LogConfig();
        public MuteRole MuteRole { get; set; } = new MuteRole();
        public EventRole EventRole { get; set; } = new EventRole();

        /// <summary>
        /// Initialises certain fields once a config object is created to ensure it works properly
        /// from the time it's created to it's deletion.
        /// </summary>
        public void InitGuildConfig()
        {
            //Initialise base parameters for every GuildConfig created.
            var fileReader = new JsonFileReader();

            //Initialise base params for every GuildConfig created.
            var json = fileReader.ReadFile(Directory.GetCurrentDirectory() + "/Json/guildParams.json");
            var paramsDict = JsonConvert.DeserializeObject<Dictionary<string, string>>(json);

            foreach (KeyValuePair<string, string> pair in paramsDict)
            {
                GuildParams.Add(new Param()
                {
                    GuildID = GuildID,
                    ShortName = pair.Key,
                    Name = pair.Value,
                    Value = ""
                });
            }

            //Initialise base channels for every GuildConfig created.
            json = fileReader.ReadFile(Directory.GetCurrentDirectory() + "/Json/channelIDs.json");
            var channelsDict = JsonConvert.DeserializeObject<Dictionary<string, string>>(json);

            foreach (KeyValuePair<string, string> pair in channelsDict)
            {
                GuildChannels.Add(new Channel()
                {
                    GuildID = GuildID,
                    ShortName = pair.Key,
                    Name = pair.Value,
                    ChannelID = default(ulong)
                });
            }

            //Initialise base logging config for every GuildConfig created.
            LogConfig = new LogConfig { GuildID = GuildID };
            MuteRole = new MuteRole { GuildID = GuildID };
            EventRole = new EventRole { GuildID = GuildID };
        }
    }
}
