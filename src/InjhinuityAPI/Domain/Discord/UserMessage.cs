﻿namespace InjhinuityAPI.Domain.Discord
{
    /// <summary>
    /// Represents a basic discord user message object.
    /// </summary>
    public class UserMessage : DiscordObject
    {
        public ulong MessageID { get; set; } = 0;
        public ulong AuthorID { get; set; } = 0;
        public string Content { get; set; } = "";
    }
}
