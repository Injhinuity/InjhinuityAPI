﻿namespace InjhinuityAPI.Domain.Discord
{
    /// <summary>
    /// Represents a basic discord text chat command object.
    /// </summary>
    public class Command : DiscordObject
    {
        public string Name { get; set; }
        public string Body { get; set; }

        public override string ToString() =>
            $"GuildID: {GuildID}, Name: {Name}, Body: {Body}";
    }
}
