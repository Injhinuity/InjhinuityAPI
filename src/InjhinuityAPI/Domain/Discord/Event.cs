﻿namespace InjhinuityAPI.Domain.Discord
{
    /// <summary>
    /// Represents a basic discord event object.
    /// </summary>
    public class Event : DiscordObject
    {
        public string Name { get; set; } = "";
        public string Description { get; set; } = "";
        public long Date { get; set; } = 0;
        public string TimeZone { get; set; } = "";
        public long Duration { get; set; } = 0;
        public bool Started { get; set; } = false;

        public override string ToString() =>
            $"GuildID: {GuildID}, Name: {Name}, Description: {Description}, " +
            $"Date: {Date}, TimeZone: {TimeZone}, Duration: {Duration}, Started: {Started}";
    }
}
