﻿namespace InjhinuityAPI.Domain.Discord
{
    /// <summary>
    /// Represents a basic discord guild logging config object.
    /// </summary>
    public class LogConfig : DiscordObject
    {
        public int LogFlagValue { get; set; }

        public override string ToString() =>
            $"GuildID: {GuildID}, Value: {LogFlagValue}";
    }
}
