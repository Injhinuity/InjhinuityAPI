﻿namespace InjhinuityAPI.Domain.Discord
{
    /// <summary>
    /// Represents a basic discord mute role object.
    /// </summary>
    public class MuteRole : DiscordObject
    {
        public string Name { get; set; }
        public ulong RoleID { get; set; }
    }
}
