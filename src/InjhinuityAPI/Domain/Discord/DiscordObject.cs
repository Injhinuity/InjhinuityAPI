﻿using InjhinuityAPI.Domain.DiscordObjects;

namespace InjhinuityAPI.Domain.Discord
{
    /// <summary>
    /// Defines an object in the model as a Discord Exclusive Object with a specific GuildID.
    /// </summary>
    public abstract class DiscordObject : IDiscordObject
    {
        public ulong GuildID { get; set; }
    }
}
