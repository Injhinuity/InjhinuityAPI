﻿namespace InjhinuityAPI.Domain.Discord
{
    /// <summary>
    /// Represents a basic discord event role object.
    /// </summary>
    public class EventRole : DiscordObject
    {
        public string Name { get; set; }
        public ulong RoleID { get; set; }
    }
}
