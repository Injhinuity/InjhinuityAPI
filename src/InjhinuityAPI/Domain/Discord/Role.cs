﻿namespace InjhinuityAPI.Domain.Discord
{
    /// <summary>
    /// Represents a basic discord role object.
    /// </summary>
    public class Role : DiscordObject
    {
        public string Name { get; set; }
        public ulong RoleID { get; set; }
    }
}
