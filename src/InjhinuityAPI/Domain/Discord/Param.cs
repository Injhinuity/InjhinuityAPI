﻿namespace InjhinuityAPI.Domain.Discord
{
    /// <summary>
    /// Represents a basic discord guild parameter object.
    /// </summary>
    public class Param : DiscordObject
    {
        public string ShortName { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
    }
}
