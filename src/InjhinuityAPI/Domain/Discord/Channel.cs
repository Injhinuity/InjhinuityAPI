﻿namespace InjhinuityAPI.Domain.Discord
{
    /// <summary>
    /// Represents a basic discord text channel object.
    /// </summary>
    public class Channel : DiscordObject
    {
        public string ShortName { get; set; }
        public string Name { get; set; }
        public ulong ChannelID { get; set; }
    }
}
