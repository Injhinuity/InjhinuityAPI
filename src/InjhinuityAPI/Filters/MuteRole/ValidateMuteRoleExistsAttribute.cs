﻿using InjhinuityAPI.Dto.Discord;
using InjhinuityAPI.Services.Controllers.Interfaces;
using Microsoft.AspNetCore.Mvc.Filters;
using System.Threading.Tasks;

namespace InjhinuityAPI.Filters
{
    /// <summary>
    /// Validation attribute for <see cref="MuteRoleDto"/> objects. Will cause any REST request
    /// to validate if said object already exists in the model layer.
    /// </summary>
    public class ValidateMuteRoleExistsAttribute : DiscordObjectExistsAttribute
    {
        public ValidateMuteRoleExistsAttribute() : base(typeof(ValidateMuteRoleExistsFilterImpl))
        {
        }

        private class ValidateMuteRoleExistsFilterImpl : DiscordObjectExistsAttributeImpl<MuteRoleDto>, IAsyncActionFilter
        {
            private readonly IMuteRoleService _muteRoleService;

            public ValidateMuteRoleExistsFilterImpl(IMuteRoleService muteRoleService)
            {
                _muteRoleService = muteRoleService;
            }

            public override async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
            {
                await ValidateGuildIDArgument(context);
                await base.OnActionExecutionAsync(context, next);
            }

            protected override async Task<bool> IsObjectMissing(MuteRoleDto @object) =>
                await IsObjectMissing(@object.GuildID, @object.Name);

            protected override async Task<bool> IsObjectMissing(ulong guildID, string param)
            {
                var muteRole = await _muteRoleService.GetAsync(guildID);
                return muteRole == null;
            }
        }
    }
}
