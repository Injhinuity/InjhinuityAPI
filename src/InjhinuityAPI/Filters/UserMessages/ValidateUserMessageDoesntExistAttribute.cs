﻿using InjhinuityAPI.Dto.Discord;
using InjhinuityAPI.Services.Controllers.Interfaces;
using InjhinuityAPI.Services;
using Microsoft.AspNetCore.Mvc.Filters;
using System.Linq;
using System.Threading.Tasks;

namespace InjhinuityAPI.Filters
{
    /// <summary>
    /// Validation attribute for <see cref="UserMessage"/> objects. Will cause any REST request
    /// to validate if said object doesn't exist in the model layer.
    /// </summary>
    public class ValidateUserMessageDoesntExistAttribute : DiscordObjectDoesntExistAttribute
    {
        public ValidateUserMessageDoesntExistAttribute() : base(typeof(ValidateUserMessageDoesntExistFilterImpl))
        {
        }

        private class ValidateUserMessageDoesntExistFilterImpl : DiscordObjectDoesntExistAttributeImpl<UserMessageDto>, IAsyncActionFilter
        {
            private readonly IUserMessageService _userMessageService;

            public ValidateUserMessageDoesntExistFilterImpl(IUserMessageService userMessageService)
            {
                _userMessageService = userMessageService;
            }

            public override async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
            {
                await ValidateGuildIDNameArguments(context);
                await base.OnActionExecutionAsync(context, next);
            }

            protected override async Task<bool> IsObjectPresent(UserMessageDto @object) =>
                await IsObjectPresent(@object.GuildID, @object.MessageID.ToString());

            protected override async Task<bool> IsObjectPresent(ulong guildID, string param)
            {
                var userMessages = await _userMessageService.GetAllAsync(guildID);
                return userMessages.Any(c => c.MessageID == ulong.Parse(param));
            }
        }
    }
}
