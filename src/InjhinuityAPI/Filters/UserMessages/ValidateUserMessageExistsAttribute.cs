﻿using System.Linq;
using System.Threading.Tasks;
using InjhinuityAPI.Dto.Discord;
using InjhinuityAPI.Services.Controllers.Interfaces;
using InjhinuityAPI.Services;
using Microsoft.AspNetCore.Mvc.Filters;

namespace InjhinuityAPI.Filters
{
    /// <summary>
    /// Validation attribute for <see cref="UserMessage"/> objects. Will cause any REST request
    /// to validate if said object already exists in the model layer.
    /// </summary>
    public class ValidateUserMessageExistsAttribute : DiscordObjectExistsAttribute
    {
        public ValidateUserMessageExistsAttribute() : base(typeof(ValidateUserMessageExistsFilterImpl))
        {
        }

        private class ValidateUserMessageExistsFilterImpl : DiscordObjectExistsAttributeImpl<UserMessageDto>, IAsyncActionFilter
        {
            private readonly IUserMessageService _userMessageService;

            public ValidateUserMessageExistsFilterImpl(IUserMessageService userMessageService)
            {
                _userMessageService = userMessageService;
            }

            public override async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
            {
                await ValidateGuildIDNameArguments(context);
                await base.OnActionExecutionAsync(context, next);
            }

            protected override async Task<bool> IsObjectMissing(UserMessageDto @object) =>
                await IsObjectMissing(@object.GuildID, @object.MessageID.ToString());

            protected override async Task<bool> IsObjectMissing(ulong guildID, string param)
            {
                var userMessages = await _userMessageService.GetAllAsync(guildID);
                return userMessages.All(c => c.MessageID != ulong.Parse(param));
            }
        }
    }
}
