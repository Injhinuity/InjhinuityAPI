﻿using InjhinuityAPI.Dto.Discord;
using InjhinuityAPI.Services.Controllers.Interfaces;
using Microsoft.AspNetCore.Mvc.Filters;
using System.Linq;
using System.Threading.Tasks;

namespace InjhinuityAPI.Filters
{
    /// <summary>
    /// Validation attribute for <see cref="Role"/> objects. Will cause any REST request
    /// to validate if said object doesn't exist in the model layer.
    /// </summary>
    public class ValidateRoleDoesntExistAttribute : DiscordObjectDoesntExistAttribute
    {
        public ValidateRoleDoesntExistAttribute() : base(typeof(ValidateRoleDoesntExistAttributeFilterImpl))
        {
        }

        private class ValidateRoleDoesntExistAttributeFilterImpl : DiscordObjectDoesntExistAttributeImpl<RoleDto>, IAsyncActionFilter
        {
            private readonly IRoleService _roleService;

            public ValidateRoleDoesntExistAttributeFilterImpl(IRoleService roleService)
            {
                _roleService = roleService;
            }

            public override async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
            {
                await ValidateGuildIDNameArguments(context);
                await base.OnActionExecutionAsync(context, next);
            }

            protected override async Task<bool> IsObjectPresent(RoleDto @object) =>
                await IsObjectPresent(@object.GuildID, @object.Name);

            protected override async Task<bool> IsObjectPresent(ulong guildID, string param)
            {
                var roles = await _roleService.GetAllAsync(guildID);
                return roles.Any(c => c.Name == param);
            }
        }
    }
}
