﻿using InjhinuityAPI.Dto.Discord;
using InjhinuityAPI.Services.Controllers.Interfaces;
using Microsoft.AspNetCore.Mvc.Filters;
using System.Linq;
using System.Threading.Tasks;

namespace InjhinuityAPI.Filters
{
    /// <summary>
    /// Validation attribute for <see cref="Role"/> objects. Will cause any REST request
    /// to validate if said object already exists in the model layer.
    /// </summary>
    public class ValidateRoleExistsAttribute : DiscordObjectExistsAttribute
    {
        public ValidateRoleExistsAttribute() : base(typeof(ValidateRoleExistsFilterImpl))
        {
        }

        private class ValidateRoleExistsFilterImpl : DiscordObjectExistsAttributeImpl<RoleDto>, IAsyncActionFilter
        {
            private readonly IRoleService _roleService;

            public ValidateRoleExistsFilterImpl(IRoleService roleService)
            {
                _roleService = roleService;
            }

            public override async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
            {
                await ValidateGuildIDNameArguments(context);
                await base.OnActionExecutionAsync(context, next);
            }

            protected override async Task<bool> IsObjectMissing(RoleDto @object) =>
                await IsObjectMissing(@object.GuildID, @object.Name);

            protected override async Task<bool> IsObjectMissing(ulong guildID, string param)
            {
                var roles = await _roleService.GetAllAsync(guildID);
                return roles.All(c => c.Name != param);
            }
        }
    }
}
