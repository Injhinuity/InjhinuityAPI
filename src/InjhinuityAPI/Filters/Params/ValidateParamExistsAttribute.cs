﻿using InjhinuityAPI.Dto.Discord;
using InjhinuityAPI.Services.Controllers.Interfaces;
using Microsoft.AspNetCore.Mvc.Filters;
using System.Linq;
using System.Threading.Tasks;

namespace InjhinuityAPI.Filters
{
    /// <summary>
    /// Validation attribute for <see cref="GuildParam"/> objects. Will cause any REST request
    /// to validate if said object already exists in the model layer.
    /// </summary>
    public class ValidateParamExistsAttribute : DiscordObjectExistsAttribute
    {
        public ValidateParamExistsAttribute() : base(typeof(ValidateParamExistsFilterImpl))
        {
        }

        private class ValidateParamExistsFilterImpl : DiscordObjectExistsAttributeImpl<ParamDto>, IAsyncActionFilter
        {
            private readonly IParamService _paramService;

            public ValidateParamExistsFilterImpl(IParamService paramService)
            {
                _paramService = paramService;
            }

            public override async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
            {
                await ValidateGuildIDNameArguments(context);
                await base.OnActionExecutionAsync(context, next);
            }

            protected override async Task<bool> IsObjectMissing(ParamDto @object) =>
                await IsObjectMissing(@object.GuildID, @object.ShortName);

            protected override async Task<bool> IsObjectMissing(ulong guildID, string param)
            {
                var @params = await _paramService.GetAllAsync(guildID);
                return @params.All(c => c.ShortName != param);
            }
        }
    }
}
