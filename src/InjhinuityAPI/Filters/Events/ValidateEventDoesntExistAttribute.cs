﻿using InjhinuityAPI.Dto.Discord;
using InjhinuityAPI.Services.Controllers.Interfaces;
using Microsoft.AspNetCore.Mvc.Filters;
using System.Linq;
using System.Threading.Tasks;

namespace InjhinuityAPI.Filters
{
    /// <summary>
    /// Validation attribute for <see cref="Event"/> objects. Will cause any REST request
    /// to validate if said object doesn't exist in the model layer.
    /// </summary>
    public class ValidateEventDoesntExistAttribute : DiscordObjectDoesntExistAttribute
    {
        public ValidateEventDoesntExistAttribute() : base(typeof(ValidateEventDoesntExistFilterImpl))
        {
        }

        private class ValidateEventDoesntExistFilterImpl : DiscordObjectDoesntExistAttributeImpl<EventDto>, IAsyncActionFilter
        {
            private readonly IEventService _eventService;

            public ValidateEventDoesntExistFilterImpl(IEventService eventService)
            {
                _eventService = eventService;
            }

            public override async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
            {
                await ValidateGuildIDNameArguments(context);
                await base.OnActionExecutionAsync(context, next);
            }

            protected override async Task<bool> IsObjectPresent(EventDto @object) =>
                await IsObjectPresent(@object.GuildID, @object.Name);

            protected override async Task<bool> IsObjectPresent(ulong guildID, string param)
            {
                var events = await _eventService.GetAllAsync(guildID);
                return events.Any(c => c.Name == param);
            }
        }
    }
}
