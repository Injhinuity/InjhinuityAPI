﻿using InjhinuityAPI.Dto.Discord;
using InjhinuityAPI.Services.Controllers.Interfaces;
using Microsoft.AspNetCore.Mvc.Filters;
using System.Linq;
using System.Threading.Tasks;

namespace InjhinuityAPI.Filters
{
    /// <summary>
    /// Validation attribute for <see cref="Event"/> objects. Will cause any REST request
    /// to validate if said object already exists in the model layer.
    /// </summary>
    public class ValidateEventExistsAttribute : DiscordObjectExistsAttribute
    {
        public ValidateEventExistsAttribute() : base(typeof(ValidateEventExistsFilterImpl))
        {
        }

        private class ValidateEventExistsFilterImpl : DiscordObjectExistsAttributeImpl<EventDto>, IAsyncActionFilter
        {
            private readonly IEventService _eventService;

            public ValidateEventExistsFilterImpl(IEventService eventService)
            {
                _eventService = eventService;
            }

            public override async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
            {
                await ValidateGuildIDNameArguments(context);
                await base.OnActionExecutionAsync(context, next);
            }

            protected override async Task<bool> IsObjectMissing(EventDto @object) =>
                await IsObjectMissing(@object.GuildID, @object.Name);

            protected override async Task<bool> IsObjectMissing(ulong guildID, string param)
            {
                var events = await _eventService.GetAllAsync(guildID);
                return events.All(c => c.Name != param);
            }
        }
    }
}
