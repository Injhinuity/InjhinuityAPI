﻿using InjhinuityAPI.Dto.Discord;
using InjhinuityAPI.Services.Controllers.Interfaces;
using Microsoft.AspNetCore.Mvc.Filters;
using System.Linq;
using System.Threading.Tasks;

namespace InjhinuityAPI.Filters
{
    /// <summary>
    /// Validation attribute for <see cref="LogConfig"/> objects. Will cause any REST request
    /// to validate if said object already exists in the model layer.
    /// </summary>
    public class ValidateLogConfigExistsAttribute : DiscordObjectExistsAttribute
    {
        public ValidateLogConfigExistsAttribute() : base(typeof(ValidateLogConfigExistsFilterImpl))
        {
        }

        private class ValidateLogConfigExistsFilterImpl : DiscordObjectExistsAttributeImpl<LogConfigDto>, IAsyncActionFilter
        {
            private readonly ILogConfigService _logConfigService;

            public ValidateLogConfigExistsFilterImpl(ILogConfigService logConfigService)
            {
                _logConfigService = logConfigService;
            }

            public override async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
            {
                await ValidateGuildIDArgument(context);
                await base.OnActionExecutionAsync(context, next);
            }

            protected override async Task<bool> IsObjectMissing(LogConfigDto @object) =>
                await IsObjectMissing(@object.GuildID, "");

            protected override async Task<bool> IsObjectMissing(ulong guildID, string param)
            {
                var logConfigs = await _logConfigService.GetAllAsync();
                return !logConfigs.Any();
            }
        }
    }
}
