﻿using InjhinuityAPI.Dto.Discord;
using InjhinuityAPI.Services.Controllers.Interfaces;
using Microsoft.AspNetCore.Mvc.Filters;
using System.Linq;
using System.Threading.Tasks;

namespace InjhinuityAPI.Filters
{
    /// <summary>
    /// Validation attribute for <see cref="Command"/> objects. Will cause any REST request
    /// to validate if said object doesn't exist in the model layer.
    /// </summary>
    public class ValidateCommandDoesntExistAttribute : DiscordObjectDoesntExistAttribute
    {
        public ValidateCommandDoesntExistAttribute() : base(typeof(ValidateCommandDoesntExistFilterImpl))
        {
        }

        private class ValidateCommandDoesntExistFilterImpl : DiscordObjectDoesntExistAttributeImpl<CommandDto> ,IAsyncActionFilter
        {
            private readonly ICommandService _commandService;

            public ValidateCommandDoesntExistFilterImpl(ICommandService commandService)
            {
                _commandService = commandService;
            }

            public override async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
            {
                await ValidateGuildIDNameArguments(context);
                await base.OnActionExecutionAsync(context, next);
            }

            protected override async Task<bool> IsObjectPresent(CommandDto @object) =>
                await IsObjectPresent(@object.GuildID, @object.Name);

            protected override async Task<bool> IsObjectPresent(ulong guildID, string param)
            { 
                var commands = await _commandService.GetAllAsync(guildID);
                return commands.Any(c => c.Name == param);              
            }
        }
    }
}
