﻿using InjhinuityAPI.Dto.Discord;
using InjhinuityAPI.Services.Controllers.Interfaces;
using Microsoft.AspNetCore.Mvc.Filters;
using System.Linq;
using System.Threading.Tasks;

namespace InjhinuityAPI.Filters
{
    /// <summary>
    /// Validation attribute for <see cref="Command"/> objects. Will cause any REST request
    /// to validate if said object already exists in the model layer.
    /// </summary>
    public class ValidateCommandExistsAttribute : DiscordObjectExistsAttribute
    {
        public ValidateCommandExistsAttribute() : base(typeof(ValidateCommandExistsFilterImpl))
        {
        }

        private class ValidateCommandExistsFilterImpl : DiscordObjectExistsAttributeImpl<CommandDto>, IAsyncActionFilter
        {
            private readonly ICommandService _commandService;

            public ValidateCommandExistsFilterImpl(ICommandService commandService)
            {
                _commandService = commandService;
            }

            public override async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
            {
                await ValidateGuildIDNameArguments(context);
                await base.OnActionExecutionAsync(context, next);
            }

            protected override async Task<bool> IsObjectMissing(CommandDto @object) =>
                await IsObjectMissing(@object.GuildID, @object.Name);

            protected override async Task<bool> IsObjectMissing(ulong guildID, string param)
            {
                var commands = await _commandService.GetAllAsync(guildID);
                return commands.All(c => c.Name != param);
            }
        }
    }
}
