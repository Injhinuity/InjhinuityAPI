﻿using InjhinuityAPI.Dto.Discord;
using InjhinuityAPI.Services.Controllers.Interfaces;
using Microsoft.AspNetCore.Mvc.Filters;
using System.Threading.Tasks;

namespace InjhinuityAPI.Filters
{
    /// <summary>
    /// Validation attribute for <see cref="EventRoleDto"/> objects. Will cause any REST request
    /// to validate if said object already exists in the model layer.
    /// </summary>
    public class ValidateEventRoleExistsAttribute : DiscordObjectExistsAttribute
    {
        public ValidateEventRoleExistsAttribute() : base(typeof(ValidateEventRoleExistsFilterImpl))
        {
        }

        private class ValidateEventRoleExistsFilterImpl : DiscordObjectExistsAttributeImpl<EventRoleDto>, IAsyncActionFilter
        {
            private readonly IEventRoleService _eventRoleService;

            public ValidateEventRoleExistsFilterImpl(IEventRoleService eventRoleService)
            {
                _eventRoleService = eventRoleService;
            }

            public override async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
            {
                await ValidateGuildIDArgument(context);
                await base.OnActionExecutionAsync(context, next);
            }

            protected override async Task<bool> IsObjectMissing(EventRoleDto @object) =>
                await IsObjectMissing(@object.GuildID, @object.Name);

            protected override async Task<bool> IsObjectMissing(ulong guildID, string param)
            {
                var eventRole = await _eventRoleService.GetAsync(guildID);
                return eventRole == null;
            }
        }
    }
}
