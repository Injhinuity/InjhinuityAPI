﻿using InjhinuityAPI.Dto.Discord;
using InjhinuityAPI.Services.Controllers.Interfaces;
using Microsoft.AspNetCore.Mvc.Filters;
using System.Linq;
using System.Threading.Tasks;

namespace InjhinuityAPI.Filters
{
    /// <summary>
    /// Validation attribute for <see cref="Channel"/> objects. Will cause any REST request
    /// to validate if said object already exists in the model layer.
    /// </summary>
    public class ValidateChannelExistsAttribute : DiscordObjectExistsAttribute
    {
        public ValidateChannelExistsAttribute() : base(typeof(ValidateCommandExistsFilterImpl))
        {
        }

        private class ValidateCommandExistsFilterImpl : DiscordObjectExistsAttributeImpl<ChannelDto>, IAsyncActionFilter
        {
            private readonly IChannelService _channelService;

            public ValidateCommandExistsFilterImpl(IChannelService channelService)
            {
                _channelService = channelService;
            }

            public override async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
            {
                await ValidateGuildIDNameArguments(context);
                await base.OnActionExecutionAsync(context, next);
            }

            protected override async Task<bool> IsObjectMissing(ChannelDto @object) =>
                await IsObjectMissing(@object.GuildID, @object.ShortName);

            protected override async Task<bool> IsObjectMissing(ulong guildID, string param)
            {
                var channels = await _channelService.GetAllAsync(guildID);
                return channels.All(c => c.ShortName != param);
            }
        }
    }
}
