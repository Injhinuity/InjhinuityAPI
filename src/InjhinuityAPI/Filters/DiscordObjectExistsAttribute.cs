﻿using InjhinuityAPI.Dto.Discord;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Threading.Tasks;

namespace InjhinuityAPI.Filters
{
    /// <summary>
    /// Base class for any <see cref="DiscordObject"/> filter. Will validate if said <see cref="DiscordObject"/> 
    /// already exists inside the model layer before entering any controller.
    /// </summary>
    public abstract class DiscordObjectExistsAttribute : TypeFilterAttribute
    {
        public DiscordObjectExistsAttribute(Type type) : base(type)
        {
        }

        public abstract class DiscordObjectExistsAttributeImpl<T> : IAsyncActionFilter where T : DiscordObjectDto
        {
            protected bool _getFound = false;

            /// <summary>
            /// Triggers just before we access a controller to execute any REST request. Will search the request's arguments 
            /// for a sent object and then validate against the model to verify if this object is indeed present. The context result
            /// dictates whether the object was found or not.
            /// </summary>
            /// <param name="context">Current action context. Contains all request arguments.</param>
            /// <param name="next">Indicates the action or the next action filter was executed. The Result field is set once the attribute's condition is met.</param>
            /// <returns>Nothing if the object is present in the model layer. <see cref="BadRequestObjectResult"/> otherwise.</returns>
            public virtual async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
            {
                var typeName = typeof(T).Name.ToLower();

                // Verify if the arguments contain the name of our object.
                // If it's present, it means the right type of json object was sent.
                if (context.ActionArguments.ContainsKey(typeName))
                {
                    // Cast into appropriate type to validate against the model. If null, it means the json is invalid.
                    if (context.ActionArguments[typeName] is T @object)
                    {
                        if (await IsObjectMissing(@object))
                            context.Result = new BadRequestObjectResult("The object you were looking for doesn't exist!");
                    }
                    else
                        context.Result = new BadRequestObjectResult("The object you were looking for doesn't exist!");
                }

                // If context.Result is not null, it means the object doesn't exist.
                // We can then return since the result is already set and will trigger the BadRequest Http error.
                if (context.Result != null)
                    return;

                await next();
            }

            /// <summary>
            /// Validates if the request URL contains the required fields (GuildID and Name)
            /// to excecute properly inside the object's controller.
            /// </summary>
            /// <param name="context">Current action context. Contains the request arguments.</param>
            /// <returns></returns>
            protected async Task ValidateGuildIDNameArguments(ActionExecutingContext context)
            {
                // Verify if the arguments contain the name of our url parameters.
                // If they are present, it means the url is valid.
                if (context.ActionArguments.ContainsKey("name") &&
                    context.ActionArguments.ContainsKey("guildID"))
                {
                    // Parse the arguments into their respective types
                    var name = context.ActionArguments["name"] as string;
                    var guildID = context.ActionArguments["guildID"] as ulong?;

                    // Check if they are valid
                    if (name == "" || !guildID.HasValue)
                        context.Result = new BadRequestObjectResult("The object you were looking for doesn't exist!");               

                    // Check if the object actually exists in the model layer
                    if (await IsObjectMissing(guildID.Value, name))
                        context.Result = new BadRequestObjectResult("The object you were looking for doesn't exist!");
                }
            }

            /// <summary>
            /// Validates if the request URL contains the required fields (GuildID) 
            /// to excecute properly inside the object's controller.
            /// </summary>
            /// <param name="context">Current action context. Contains the request arguments.</param>
            /// <returns></returns>
            protected async Task ValidateGuildIDArgument(ActionExecutingContext context)
            {
                //Verify if the arguments contain the name of our url parameter.
                //If it's present, it means the url is valid.
                if (context.ActionArguments.ContainsKey("guildID"))
                {
                    //Parse the arguments into their respective types
                    var guildID = context.ActionArguments["guildID"] as ulong?;

                    //Check if they are valid
                    if (!guildID.HasValue)
                        context.Result = new BadRequestObjectResult("The object you were looking for doesn't exist!");

                    //Check if the object actually exists in the model layer
                    if (await IsObjectMissing(guildID.Value, ""))
                        context.Result = new BadRequestObjectResult("The object you were looking for doesn't exist!");
                }
            }

            /// <summary>
            /// Validates against the model to check if the object doesn't exist. Uses a single
            /// parsed json object taken from the received request.
            /// </summary>
            /// <param name="object">Object to find.</param>
            /// <returns>True if it doesn't exist, false otherwise.</returns>
            protected abstract Task<bool> IsObjectMissing(T @object);

            /// <summary>
            /// Validates against the model to check if the object doesn't exist. Uses url
            /// parameters parsed from the received url arguments.
            /// </summary>
            /// <param name="guildID">GuildID of object to find.</param>
            /// <param name="param">Name of object to find.</param>
            /// <returns>True if it doesn't exist, false otherwise.</returns>
            protected abstract Task<bool> IsObjectMissing(ulong guildID, string param);
        }
    }
}
