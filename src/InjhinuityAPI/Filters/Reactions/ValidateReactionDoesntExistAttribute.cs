﻿using InjhinuityAPI.Dto.Discord;
using InjhinuityAPI.Services.Controllers.Interfaces;
using Microsoft.AspNetCore.Mvc.Filters;
using System.Linq;
using System.Threading.Tasks;

namespace InjhinuityAPI.Filters
{
    /// <summary>
    /// Validation attribute for <see cref="Reaction"/> objects. Will cause any REST request
    /// to validate if said object doesn't exist in the model layer.
    /// </summary>
    public class ValidateReactionDoesntExistAttribute : DiscordObjectDoesntExistAttribute
    {
        public ValidateReactionDoesntExistAttribute() : base(typeof(ValidateReactionDoesntExistFilterImpl))
        {
        }

        private class ValidateReactionDoesntExistFilterImpl : DiscordObjectDoesntExistAttributeImpl<ReactionDto>, IAsyncActionFilter
        {
            private readonly IReactionService _reactionService;

            public ValidateReactionDoesntExistFilterImpl(IReactionService reactionService)
            {
                _reactionService = reactionService;
            }

            public override async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
            {
                await ValidateGuildIDNameArguments(context);
                await base.OnActionExecutionAsync(context, next);
            }

            protected override async Task<bool> IsObjectPresent(ReactionDto @object) =>
                await IsObjectPresent(@object.GuildID, @object.Name);

            protected override async Task<bool> IsObjectPresent(ulong guildID, string param)
            {
                var reactions = await _reactionService.GetAllAsync(guildID);
                return reactions.Any(c => c.Name == param);
            }
        }
    }
}
