﻿using InjhinuityAPI.Dto.Discord;
using InjhinuityAPI.Services.Controllers.Interfaces;
using Microsoft.AspNetCore.Mvc.Filters;
using System.Linq;
using System.Threading.Tasks;

namespace InjhinuityAPI.Filters
{
    /// <summary>
    /// Validation attribute for <see cref="Reaction"/> objects. Will cause any REST request
    /// to validate if said object already exists in the model layer.
    /// </summary>
    public class ValidateReactionExistsAttribute : DiscordObjectExistsAttribute
    {
        public ValidateReactionExistsAttribute() : base(typeof(ValidateReactionExistsFilterImpl))
        {
        }

        private class ValidateReactionExistsFilterImpl : DiscordObjectExistsAttributeImpl<ReactionDto>, IAsyncActionFilter
        {
            private readonly IReactionService _reactionService;

            public ValidateReactionExistsFilterImpl(IReactionService reactionService)
            {
                _reactionService = reactionService;
            }

            public override async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
            {
                await ValidateGuildIDNameArguments(context);
                await base.OnActionExecutionAsync(context, next);
            }

            protected override async Task<bool> IsObjectMissing(ReactionDto @object) =>
                await IsObjectMissing(@object.GuildID, @object.Name);

            protected override async Task<bool> IsObjectMissing(ulong guildID, string param)
            {
                var roles = await _reactionService.GetAllAsync(guildID);
                return roles.All(c => c.Name != param);
            }
        }
    }
}
