﻿using InjhinuityAPI.Services.Provider;
using InjhinuityAPI.Extentions;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.Reflection;
using Microsoft.Extensions.Logging;
using NLog.Config;
using NLog.Targets;
using NLog;

namespace InjhinuityAPI
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration) =>
            Configuration = configuration;

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            InitLogger();

            services.AddMvcCore()
                .AddAuthorization()
                .AddJsonFormatters();

            // Adds a route to the identification server which provides oAuth tokens for
            // API access.
            services.AddAuthentication("Bearer")
                .AddIdentityServerAuthentication(options =>
                {
                    options.Authority = "http://localhost:4000";
                    options.RequireHttpsMetadata = false;
                    options.ApiName = "InjhinuityApi";
                });

            // Injects all of the custom services into the API.
            var customServices = new BotServiceCollectionBuilder()
                .LoadFrom(Assembly.GetEntryAssembly());

            services.AddMultipleFromDict(customServices);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            app.UseAuthentication();
            app.UseMvc();
        }

        /// <summary>
        /// Creates our console target and format for logging purposes.
        /// </summary>
        private void InitLogger()
        {
            var logConfig = new LoggingConfiguration();
            var consoleTarget = new ColoredConsoleTarget()
            {
                Layout = @"${date:format=HH\:mm\:ss} ${logger} | ${message}"
            };
            logConfig.AddTarget("Console", consoleTarget);

            logConfig.LoggingRules.Add(new LoggingRule("*", NLog.LogLevel.Debug, consoleTarget));

            LogManager.Configuration = logConfig;
        }
    }
}
