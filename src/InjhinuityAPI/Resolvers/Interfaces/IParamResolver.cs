﻿using InjhinuityAPI.Services.Injection;

namespace InjhinuityAPI.Resolvers.Interfaces
{
    public interface IParamResolver : IService
    {
        string GetParam(string param);
        bool TryGetParam(string param, out string paramName);
    }
}
