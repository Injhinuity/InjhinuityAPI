﻿using InjhinuityAPI.Services.Injection;

namespace InjhinuityAPI.Resolvers.Interfaces
{
    public interface INameResolver : IService
    {
        string GetName(string name);
        bool TryGetName(string name, out string paramName);
    }
}
