﻿using InjhinuityAPI.Database.Config;
using InjhinuityAPI.Resolvers.Interfaces;
using InjhinuityAPI.Services.Interfaces;

namespace InjhinuityAPI.Resolvers
{
    /// <summary>
    /// Resolves full <see cref="GuildParam"/> display names from ./Json/guildParams.json
    /// </summary>
    public class GuildParameterResolver : Resolver, IParamResolver
    {
        public GuildParameterResolver(IFileReader fileReader, IAPIConfig config) : base(fileReader, config) 
            => InitializeParameters(_config.ParamsPath);

        public string GetParam(string param)
            => _parameters[param];

        public bool TryGetParam(string param, out string paramName)
            => _parameters.TryGetValue(param, out paramName);
    }
}
