﻿using InjhinuityAPI.Database.Config;
using InjhinuityAPI.Resolvers.Interfaces;
using InjhinuityAPI.Services.Interfaces;

namespace InjhinuityAPI.Resolvers
{    
    /// <summary>
     /// Resolves full <see cref="Channel"/> display names from ./Json/channelIDs.json
     /// </summary>
    public class ChannelNameResolver : Resolver, INameResolver
    {
        public ChannelNameResolver(IFileReader fileReader, IAPIConfig config) : base(fileReader, config)
            => InitializeParameters(_config.ChannelNamePath);

        public string GetName(string name)
            => _parameters[name];

        public bool TryGetName(string name, out string paramName)
            => _parameters.TryGetValue(name, out paramName);
    }
}
