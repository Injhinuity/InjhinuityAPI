﻿using InjhinuityAPI.Database.Config;
using InjhinuityAPI.Services.Interfaces;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;

namespace InjhinuityAPI.Resolvers
{
    /// <summary>
    /// Generic class for parameter or name resolving. Takes abreviations and converts them
    /// into full readable names for display and queries.
    /// </summary>
    public abstract class Resolver
    {
        protected Dictionary<string, string> _parameters;
        protected readonly IFileReader _jsonReader;
        protected readonly IAPIConfig _config;

        public Resolver(IFileReader fileReader, IAPIConfig config)
        {
            _config = config;
            _jsonReader = fileReader;
        }

        protected void InitializeParameters(string filePath)
        {
            try
            {
                var json = _jsonReader.ReadFile(Directory.GetCurrentDirectory() + filePath);
                _parameters = JsonConvert.DeserializeObject<Dictionary<string, string>>(json);
            }
            catch (Exception ex)
            {
                throw new JsonReaderException($"Failed reading JSON file at {_config.ChannelNamePath}.\n\n Exception: {ex.Message}");
            }
        }
    }
}
