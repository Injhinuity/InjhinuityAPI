# JhinBotAPI
This RESTful API currently serves as a data provider for my own personnal Discord Bot Client ([Injhinuity](https://github.com/MathieuPomerleau/InjhinuityDiscordClient)). It acts as an abstraction layer between client applications and the data they consume.

## What does it do?
It's only purpose is to act as a model layer for all data storage/access purposes. Clients make RESTful requests to consume data and the API will supply with the requested data, as long as authentication succeeds.

As of now, it handles all actions a standard RESTful service would be expected to provide. As more configurations or objects are added, it will keep expanding, as there could be a possibility of developping clients for other applications via multiple routes inside the service. Logic is kept minimal, as it's goal is to provide raw data to multiple clients without replicating the same logic everywhere.  

## Extra features
Authentication is handled through another one of my Web apps running the Identity Server 4 library ([Link](https://github.com/MathieuPomerleau/IdentityServer)). The API authenticates against the service with a client secret to provide an access token for resources within the RESTful API. This authentication could eventually be split amongst different clients/applications as more clients will (hopefully) be added.
